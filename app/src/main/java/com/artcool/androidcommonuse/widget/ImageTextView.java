package com.artcool.androidcommonuse.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.artcool.androidcommonuse.R;

/**
 * 图文控件，主要用于底部导航栏
 * @author wuyb
 */
public class ImageTextView extends LinearLayout {

    private ImageView mImage;
    private TextView mText;

    public ImageTextView(Context context) {
        super(context);
        initialize(context);
    }

    public ImageTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public ImageTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    /**
     * 初始化UI
     * @param context
     */
    private void initialize(Context context) {
        View view = inflate(context, R.layout.widget_imagetext_layout, this);
        mImage = view.findViewById(R.id.ivImg);
        mText = view.findViewById(R.id.tvText);
    }

    /**
     * 设置图片
     * @param resID
     */
    public void setImage(int resID) {
        mImage.setImageResource(resID);
    }

    /**
     * 设置图片伸缩类型
     * @param type
     */
    public void setImageScaleType(ImageView.ScaleType type){
        mImage.setScaleType(type);
    }

    /**
     * 设置文字
     * @param stringID
     */
    public void setText(int stringID){
        mText.setText(stringID);
    }


}

