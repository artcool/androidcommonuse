package com.artcool.androidcommonuse.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.artcool.androidcommonuse.R;

/**
 * 圆形头像图文控件
 *
 *  --------    Title
 *  |      |    subTitile
 *  --------
 * Created by wuyibin on 2018/10/25.
 */
public class RoundImageTextView extends LinearLayout {
    private RoundImageView roundImageView;
    private TextView mTitle,mSubTitle;

    public RoundImageTextView(Context context) {
        super(context);
        initialize(context);
    }

    public RoundImageTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
        TypedArray params=context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.RoundImageTextView,
                0,0
        );
        try{
            Drawable drawable=params.getDrawable(R.styleable.RoundImageTextView_imageSrc);
            if (drawable==null) {
                drawable=getResources().getDrawable(R.mipmap.male);
            }
            roundImageView.setImageDrawable(drawable);
            mTitle.setText(params.getText(R.styleable.RoundImageTextView_imageTitle));
            mSubTitle.setText(params.getText(R.styleable.RoundImageTextView_imageSubTitle));
        }
        finally {
            params.recycle();
        }
    }

    private void initialize(Context context){
        View view = inflate(context, R.layout.widget_round_image_text_layout, this);
        roundImageView=(RoundImageView)view.findViewById(R.id.rivImage);
        mTitle=(TextView)view.findViewById(R.id.tvTitle);
        mSubTitle=(TextView)view.findViewById(R.id.tvSubTitle);
    }

    public void setRoundImageView(int resId) {
        roundImageView.setImageResource(resId);
    }

    public void setTitleText(String text) {
        mTitle.setText(text);
    }

    public void setTitleText(int resId) {
        mTitle.setText(resId);
    }

    public void setSubTitleText(String text) {
        mSubTitle.setText(text);
    }

    public void setSubTitleText(int resId) {
        mSubTitle.setText(resId);
    }
}
