package com.artcool.androidcommonuse.application;

import android.app.Application;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

/**
 * Created by wuyibin on 2018/10/25.
 */
public class CommonUseApplication extends Application {

    private static CommonUseApplication mApplication = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = this;
        //初始化logger
        Logger.addLogAdapter(new AndroidLogAdapter());
    }

    public static CommonUseApplication getInstance() {
        return mApplication;
    }
}
