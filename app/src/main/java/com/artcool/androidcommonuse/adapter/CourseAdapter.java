package com.artcool.androidcommonuse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.artcool.androidcommonuse.model.recommand.RecommandBodyValue;
import com.artcool.commonusesdk.imageloader.ImageLoaderManager;

import java.util.List;

/**
 * 首页Adapter
 *
 * @author wuyibin
 * @date 2018/11/1
 */
public class CourseAdapter extends BaseAdapter {

    /**
     * 条目类型总数
     */
    private static final int CARD_COUNT = 4;
    /**
     * 视频类型
     */
    private static final int VIDEO_TYPE = 0x00;

    private final LayoutInflater mInflater;
    private final ImageLoaderManager mImageLoader;
    private Context mContext;
    private List<RecommandBodyValue> mData;

    public CourseAdapter(Context context, List<RecommandBodyValue> list) {
        this.mContext = context;
        this.mData = list;
        mInflater = LayoutInflater.from(context);
        mImageLoader = ImageLoaderManager.getInstance(context);
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Object getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getViewTypeCount() {
        return CARD_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        RecommandBodyValue value = (RecommandBodyValue) getItem(position);
        return value.type;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        int type = getItemViewType(position);
        RecommandBodyValue value = (RecommandBodyValue) getItem(position);
        if (convertView == null) {

        } else {
            convertView.getTag();
        }
        return convertView;
    }

    private static class ViewHolder{

    }
}
