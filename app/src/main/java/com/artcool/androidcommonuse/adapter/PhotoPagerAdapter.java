package com.artcool.androidcommonuse.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.artcool.commonusesdk.imageloader.ImageLoaderManager;
import com.artcool.commonusesdk.widget.photoview.PhotoView;

import java.util.ArrayList;

/**
 *
 * @author wuyibin
 * @date 2019/7/29
 */
public class PhotoPagerAdapter extends PagerAdapter {

    private Context mContext;

    private ArrayList<String> mList;
    private ImageLoaderManager mLoader;
    private int currentPosition;


    public PhotoPagerAdapter(Context context, ArrayList<String> list) {
        this.mContext = context;
        this.mList = list;
        this.mLoader = ImageLoaderManager.getInstance(mContext);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        currentPosition = position;
        ImageView photoView = new PhotoView(mContext);
        photoView.setScaleType(ImageView.ScaleType.FIT_XY);
        mLoader.displayImage(photoView,mList.get(currentPosition));
        container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return photoView;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }
}
