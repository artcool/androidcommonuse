package com.artcool.androidcommonuse.service.update;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.artcool.androidcommonuse.R;

import java.io.File;

/**
 *  下载更新应用服务
 * @author wuyibin
 * @date 2019/7/15
 */
public class UpdateService extends Service {

    /**
     * 服务器提供的apk下载地址
     */
    private static final String APK_DOWNLOAD_URL = "http://your address/xxx.apk";

    /**
     * 文件存放路径
     */
    private String filePath;

    private NotificationManager notificationManager;
    private Notification mNotification;

    @Override
    public void onCreate() {
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        filePath = Environment.getExternalStorageDirectory() + "/file/xxx.apk";
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        notifyUser("开始下载","开始下载",0);
        startDownload();
        return super.onStartCommand(intent, flags, startId);
    }

    private void startDownload() {
        UpdateManager.getInstance().startDownload(APK_DOWNLOAD_URL, filePath, new UpdateDownloadListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onPrepeared(long contentLength, String downloadUrl) {

            }

            @Override
            public void onProgressChanged(int progress, String downloadUrl) {
                notifyUser("正在下载","正在下载",progress);
            }

            @Override
            public void onPause(int progress, int completeSize, String downloadUrl) {
                notifyUser("下载失败","下载失败，请检查网络连接或SD卡存储空间",0);
                deleteApkFile();
                stopSelf();
            }

            @Override
            public void onFinished(int completeSize, String downloadUrl) {
                notifyUser("下载完成","下载完成",0);
                //关闭自身服务
                stopSelf();
                startActivity(getInstallAplIntent());

            }

            @Override
            public void onFailure() {
                notifyUser("下载失败","下载失败,请检查网络连接或SD卡存储空间",0);
                deleteApkFile();
                stopSelf();
            }
        });
    }

    /**
     * 删除apk文件
     */
    private boolean deleteApkFile() {
        File file = new File(filePath);
        if (file.exists() && file.isFile()) {
            return file.delete();
        }
        return false;
    }

    public void notifyUser(String tickerMessage,String message,int progress) {
        notifyThatExceed(tickerMessage,message,progress);
    }

    private void notifyThatExceed(String tickerMessage, String message, int progress) {
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this);
        //小图
        notification.setSmallIcon(R.mipmap.ic_launcher);
        //大图
        notification.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher));
        notification.setContentTitle("内容标题");
        if (progress > 0 && progress < 100) {
            notification.setProgress(100,progress,false);
        } else {
            /**
             * 0,0,false,可以将进度条隐藏
             */
            notification.setProgress(0,0,false);
            notification.setContentTitle(message);
        }
        notification.setAutoCancel(true);
        notification.setWhen(System.currentTimeMillis());
        notification.setTicker(tickerMessage);
        notification.setContentIntent(progress >= 100 ? getContentIntent() : PendingIntent.getActivity(this,0,new Intent(),PendingIntent.FLAG_UPDATE_CURRENT));
        mNotification = notification.build();
        notificationManager.notify(0,mNotification);
    }

    private PendingIntent getContentIntent() {
        PendingIntent contentIntent = PendingIntent.getActivity(this,0,getInstallAplIntent(),PendingIntent.FLAG_UPDATE_CURRENT);
        return contentIntent;
    }

    private Intent getInstallAplIntent() {
        File apkFile = new File(filePath);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(Uri.parse("file://" + apkFile.toString()),"application/vnd.android.package-archive");
        return intent;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
