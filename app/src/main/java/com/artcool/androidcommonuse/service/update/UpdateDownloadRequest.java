package com.artcool.androidcommonuse.service.update;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;

/**
 * @author wuyibin
 * @date 2019/7/15
 */
public class UpdateDownloadRequest implements Runnable {
    private int startPos = 0;
    private String downloadUrl;
    private int contentLength;
    private String localFilePath;
    private UpdateDownloadListener downloadListener;
    private DownloadResponseHandler downloadHandler;
    private boolean isDownloading = false;

    public UpdateDownloadRequest(String downloadUrl,  String localFilePath, UpdateDownloadListener downloadListener) {
        this.downloadUrl = downloadUrl;
        this.localFilePath = localFilePath;
        this.downloadListener = downloadListener;
        this.downloadHandler = new DownloadResponseHandler();
        this.isDownloading = true;
    }

    @Override
    public void run() {
        makeRequest();
    }

    private void makeRequest() {
        if (!Thread.currentThread().isInterrupted()) {
            try {
                URL url = new URL(downloadUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(5000);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Range", "byte=" + startPos + "-");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.connect();
                contentLength = connection.getContentLength();
                if (!Thread.currentThread().isInterrupted()) {
                    if (downloadHandler != null) {
                        downloadHandler.sendResponseMessage(connection.getInputStream());
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isDownloading() {
        return isDownloading;
    }

    public void stopDownloading() {
        isDownloading = false;
    }

    public enum FailureCode {
        UNKNOWNHOST,
        SOCKET,
        SOCKETTIMEOUT,
        CONNECTTIMEOUT,
        IO,
        HTTPRESPONSE,
        JSON,
        INTERRUPTED
    }

    public class DownloadResponseHandler {
        protected static final int SUCCESS_MESSAGE = 0;
        protected static final int FAILURE_MESSAGE = 1;
        protected static final int START_MESSAGE = 2;
        protected static final int FINISH_MESSAGE = 3;
        protected static final int NETWORK_OFF = 4;
        private static final int PROGRESS_CHANGED = 5;
        private static final int PAUSED_MESSAGE = 7;
        private int mCompleteSize = 0;
        private int progress = 0;
        private Handler handler;

        public DownloadResponseHandler() {
            if (Looper.myLooper() != null) {
                handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        handleSelfMessage(msg);
                    }
                };
            }
        }

        private void handleSelfMessage(Message msg) {
            Object[] response;
            switch (msg.what) {
                case FAILURE_MESSAGE:
                    response = (Object[]) msg.obj;
                    handleFailureMessage((FailureCode) response[0]);
                    break;
                case PROGRESS_CHANGED:
                    response = (Object[]) msg.obj;
                    handleProgressChangeMessage(((Integer) response[0]).intValue());
                    break;
                case PAUSED_MESSAGE:
                    handlePauseMessage();
                    break;
                case FINISH_MESSAGE:
                    onFinish();
                default:
                    break;
            }
        }

        private void onFinish() {
            downloadListener.onFinished(mCompleteSize, "");
        }

        private void handlePauseMessage() {
            downloadListener.onPause(progress, mCompleteSize, "");
        }

        private void handleProgressChangeMessage(int intValue) {
            downloadListener.onProgressChanged(intValue, "");
        }

        private void handleFailureMessage(FailureCode failureCode) {
            onFailure(failureCode);
        }

        private void onFailure(FailureCode failureCode) {
            downloadListener.onFailure();
        }

        public void sendResponseMessage(InputStream inputStream) {
            RandomAccessFile randomAccessFile = null;
            mCompleteSize = 0;
            try {
                byte[] buffer = new byte[1024];
                int length = -1;
                int limit = 0;
                randomAccessFile = new RandomAccessFile(localFilePath, "rwd");
                randomAccessFile.seek(startPos);
                boolean isPaused = false;
                while ((length = inputStream.read(buffer)) != -1) {
                    if (isDownloading) {
                        randomAccessFile.write(buffer, 0, length);
                        mCompleteSize += length;
                        if ((startPos + mCompleteSize) < (contentLength + startPos)) {
                            progress = (int) (Float.parseFloat(getTwoPointFloatStr((float) (startPos + mCompleteSize) / (contentLength + startPos))) * 100);
                            if (limit % 30 == 0 || progress == 100) {
                                //在子线程中读取流数据后转发到主线程中去显示UI
                                sendProgressChangedMessage(progress);
                            }
                        }
                        limit++;
                    } else {
                        isPaused = true;
                        sendPausedMessage();
                        break;
                    }
                }
                stopDownloading();
                if (isPaused) {
                    sendFinishMessage();
                }
            }  catch (IOException e) {
                sendPausedMessage();
                stopDownloading();
                e.printStackTrace();
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    if (randomAccessFile != null) {
                        randomAccessFile.close();
                    }
                } catch (IOException e) {
                    stopDownloading();
                    e.printStackTrace();
                }
            }
        }

        protected void sendFinishMessage() {
            sendMessage(obtainMessage(FINISH_MESSAGE,null));
        }


        private void sendPausedMessage() {
            sendMessage(obtainMessage(PAUSED_MESSAGE,null));
        }

        private void sendProgressChangedMessage(int progress) {
            sendMessage(obtainMessage(PROGRESS_CHANGED,new Object[]{progress}));
        }

        private void sendMessage(Message message) {
            if (handler != null) {
                handler.sendMessage(message);
            } else {
                handleSelfMessage(message);
            }
        }

        private Message obtainMessage(int progressChanged, Object object) {
            Message msg = null;
            if (handler != null) {
                msg = this.handler.obtainMessage(progressChanged,object);
            } else {
                msg = Message.obtain();
                msg.what = progressChanged;
                msg.obj = object;
            }
            return msg;
        }

        private String getTwoPointFloatStr(float value) {
            DecimalFormat df = new DecimalFormat("0.00");
            return df.format(value);
        }
    }
}
