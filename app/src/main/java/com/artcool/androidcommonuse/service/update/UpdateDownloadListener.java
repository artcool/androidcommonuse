package com.artcool.androidcommonuse.service.update;

/**
 *  应用更新下载监听器
 * @author wuyibin
 * @date 2019/7/15
 */
public interface UpdateDownloadListener {

    /**
     * 下载请求开始回调
     */
    void onStart();

    /**
     * 请求成功，下载前的准备回调
     * @param contentLength
     * @param downloadUrl
     */
    void onPrepeared(long contentLength,String downloadUrl);

    /**
     * 进度更新回调
     * @param progress
     * @param downloadUrl
     */
    void onProgressChanged(int progress,String downloadUrl);

    /**
     * 下载过程中暂停的回调
     * @param progress
     * @param completeSize
     * @param downloadUrl
     */
    void onPause(int progress,int completeSize,String downloadUrl);

    /**
     * 下载完成回调
     * @param completeSize
     * @param downloadUrl
     */
    void onFinished(int completeSize,String downloadUrl);

    /**
     * 下载失败回调
     */
   void onFailure();

}
