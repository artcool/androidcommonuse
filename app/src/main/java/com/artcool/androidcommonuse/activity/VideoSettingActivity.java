package com.artcool.androidcommonuse.activity;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.artcool.androidcommonuse.R;
import com.artcool.androidcommonuse.activity.base.BaseActivity;
import com.artcool.androidcommonuse.db.SharedPreferenceManager;
import com.artcool.commonusesdk.constant.SDKConstant;
import com.artcool.commonusesdk.core.Parameters;

/**
 * 视频播放设置页面
 *
 * @author wuyibin
 * @date 2018/12/28
 */
public class VideoSettingActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mBack;
    private RelativeLayout mAlway;
    private RelativeLayout mWifi;
    private RelativeLayout mClose;
    private CheckBox mAlwayBox, mWifiBox, mCloseBox;

    @Override
    protected void initListening() {

    }

    @Override
    protected void initData() {
        int currentSetting = SharedPreferenceManager.getInstance().getInt(SharedPreferenceManager.VIDEO_PLAY_SETTING, 1);
        switch (currentSetting) {
            case 0:
                mAlwayBox.setBackgroundResource(R.mipmap.setting_selected);
                mWifiBox.setBackgroundResource(0);
                mCloseBox.setBackgroundResource(0);
                break;
            case 1:
                mWifiBox.setBackgroundResource(R.mipmap.setting_selected);
                mAlwayBox.setBackgroundResource(0);
                mCloseBox.setBackgroundResource(0);
                break;
            case 2:
                mCloseBox.setBackgroundResource(R.mipmap.setting_selected);
                mWifiBox.setBackgroundResource(0);
                mAlwayBox.setBackgroundResource(0);
                break;
            default:
                break;

        }
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_video_setting);
        mBack = findViewById(R.id.back_view);
        mAlway = findViewById(R.id.alway_layout);
        mWifi = findViewById(R.id.wifi_layout);
        mClose = findViewById(R.id.close_layout);
        mAlwayBox = findViewById(R.id.alway_check_box);
        mWifiBox = findViewById(R.id.wifi_check_box);
        mCloseBox = findViewById(R.id.close_check_box);
        mBack.setOnClickListener(this);
        mAlway.setOnClickListener(this);
        mWifi.setOnClickListener(this);
        mClose.setOnClickListener(this);
    }

    @Override
    protected void initVariable() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.alway_layout:
                SharedPreferenceManager.getInstance().putInt(SharedPreferenceManager.VIDEO_PLAY_SETTING,0);
                Parameters.setCurrentSetting(SDKConstant.AutoPlaySetting.AUTO_PLAY_3G_4G_WIFI);
                mAlwayBox.setBackgroundResource(R.mipmap.setting_selected);
                mWifiBox.setBackgroundResource(0);
                mCloseBox.setBackgroundResource(0);
                break;
            case R.id.wifi_layout:
                SharedPreferenceManager.getInstance().putInt(SharedPreferenceManager.VIDEO_PLAY_SETTING,1);
                Parameters.setCurrentSetting(SDKConstant.AutoPlaySetting.AUTO_PLAY_ONLY_WIFI);
                mWifiBox.setBackgroundResource(R.mipmap.setting_selected);
                mAlwayBox.setBackgroundResource(0);
                mCloseBox.setBackgroundResource(0);
                break;
            case R.id.close_layout:
                SharedPreferenceManager.getInstance().putInt(SharedPreferenceManager.VIDEO_PLAY_SETTING,2);
                Parameters.setCurrentSetting(SDKConstant.AutoPlaySetting.AUTO_PLAY_NEVER);
                mCloseBox.setBackgroundResource(R.mipmap.setting_selected);
                mWifiBox.setBackgroundResource(0);
                mAlwayBox.setBackgroundResource(0);
                break;
            case R.id.back_view:
                finish();
                break;
            default:
                break;
        }
    }
}
