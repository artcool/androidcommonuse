package com.artcool.androidcommonuse.activity;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.View;

import com.artcool.androidcommonuse.R;
import com.artcool.androidcommonuse.activity.base.BaseActivity;
import com.artcool.androidcommonuse.manager.UserManager;
import com.artcool.androidcommonuse.model.user.User;
import com.artcool.androidcommonuse.network.http.RequestCenter;
import com.artcool.commonusesdk.okhttp.listener.DisposeDataListener;

public class LoginActivity extends BaseActivity {

    public static final String LOGIN_ACTION = "com.artcool.action.LOGIN_ACTION";
    private AppCompatEditText mUserName;
    private AppCompatEditText mPassword;
    private AppCompatButton mLogin;

    @Override
    protected void initListening() {
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    /**
     * 发送登录请求
     */
    private void login() {
        String username = mUserName.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        if (TextUtils.isEmpty(username)) {
            return;
        }
        if (TextUtils.isEmpty(password)) {
            return;
        }

        RequestCenter.login(username, password, new DisposeDataListener() {
            @Override
            public void onSuccess(Object responseObj) {
                User user = (User) responseObj;
                UserManager.getInstance().setUser(user);
                sendLoginBroadcast();
            }

            @Override
            public void onFailure(Object reasonObj) {

            }
        });

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_login);
        mUserName = findViewById(R.id.et_login_username);
        mPassword = findViewById(R.id.et_login_pwd);
        mLogin = findViewById(R.id.btn_login_login);
    }

    @Override
    protected void initVariable() {

    }

    private void sendLoginBroadcast () {
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(LOGIN_ACTION));
    }
}
