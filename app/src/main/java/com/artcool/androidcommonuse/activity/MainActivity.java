package com.artcool.androidcommonuse.activity;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.artcool.androidcommonuse.R;
import com.artcool.androidcommonuse.activity.base.BaseActivity;
import com.artcool.androidcommonuse.fragment.HomeFragment;
import com.artcool.androidcommonuse.fragment.MessageFragment;
import com.artcool.androidcommonuse.fragment.MineFragment;
import com.artcool.androidcommonuse.fragment.VideoFragment;
import com.artcool.androidcommonuse.widget.BottomNavigationViewEx;
import com.artcool.commonusesdk.imageloader.ImageLoaderManager;
import com.orhanobut.logger.Logger;

public class MainActivity extends BaseActivity {

    private BottomNavigationViewEx mBar;
    private FrameLayout mContainer;
    private HomeFragment mHomeFragment;
    private VideoFragment mVideoFragment;
    private MessageFragment mMessageFragment;
    private MineFragment mMineFragment;
    private FragmentManager fm;

    @Override
    protected void initListening() {
        mBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                FragmentTransaction transaction = fm.beginTransaction();
                switch (menuItem.getItemId()) {
                    case R.id.iHome:
                        hideFragment(mVideoFragment,transaction);
                        hideFragment(mMessageFragment,transaction);
                        hideFragment(mMineFragment,transaction);
                        if (mHomeFragment == null) {
                            mHomeFragment = new HomeFragment();
                            transaction.add(R.id.container,mHomeFragment);
                        } else {
                            transaction.show(mHomeFragment);
                        }
                        break;
                    case R.id.iVedio:
                        hideFragment(mHomeFragment,transaction);
                        hideFragment(mMessageFragment,transaction);
                        hideFragment(mMineFragment,transaction);
                        if (mVideoFragment == null) {
                            mVideoFragment = new VideoFragment();
                            transaction.add(R.id.container,mVideoFragment);
                        } else {
                            transaction.show(mVideoFragment);
                        }
                        break;
                    case R.id.iMessage:
                        hideFragment(mVideoFragment,transaction);
                        hideFragment(mHomeFragment,transaction);
                        hideFragment(mMineFragment,transaction);
                        if (mMessageFragment == null) {
                            mMessageFragment = new MessageFragment();
                            transaction.add(R.id.container,mMessageFragment);
                        } else {
                            transaction.show(mMessageFragment);
                        }
                        break;
                    case R.id.iMine:
                        hideFragment(mVideoFragment,transaction);
                        hideFragment(mMessageFragment,transaction);
                        hideFragment(mHomeFragment,transaction);
                        if (mMineFragment == null) {
                            mMineFragment = new MineFragment();
                            transaction.add(R.id.container,mMineFragment);
                        } else {
                            transaction.show(mMineFragment);
                        }
                        break;
                        default:
                            break;
                }
                transaction.commit();
                return false;
            }
        });
    }

    private void hideFragment(Fragment fragment, FragmentTransaction transaction) {
        if (fragment != null) {
            transaction.hide(fragment);
        }
    }

    @Override
    protected void initData() {
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_main);
        mBar = findViewById(R.id.bnvBar);
        mBar.enableAnimation(false);
        //显示图标及文字
        mBar.enableShiftingMode(false);
        mBar.enableItemShiftingMode(false);
        mContainer = findViewById(R.id.container);

        initFragment();
    }

    private void initFragment() {
        mHomeFragment = new HomeFragment();
        fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container,mHomeFragment);
        ft.commit();
    }

    @Override
    protected void initVariable() {

    }
}
