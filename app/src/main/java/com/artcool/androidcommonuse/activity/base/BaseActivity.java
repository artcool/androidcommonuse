package com.artcool.androidcommonuse.activity.base;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.artcool.androidcommonuse.constants.Constans;

/**
 *
 * @author wuyibin
 * @date 2018/10/25
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVariable();
        initView();
        initData();
        initListening();
    }

    /**
     * 控件事件监听
     */
    protected abstract void initListening();

    /**
     * 获取数据
     */
    protected abstract void initData();

    /**
     * 初始化UI
     */
    protected abstract void initView();

    /**
     * 初始化页面参数
     */
    protected abstract void initVariable();


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * 申请特定权限
     *
     * @param code
     * @param permissions
     */
    public void requestPermission(int code, String... permissions) {
        ActivityCompat.requestPermissions(this, permissions, code);
    }

    public boolean hasPermission(String... permissions) {
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this,permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constans.WRITE_READ_EXTERNAL_CODE:
                if (grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    doSomething();
                }
                break;
        }
    }

    /**
     * 处理获取权限后的逻辑
     */
    public void doSomething(){

    }
}
