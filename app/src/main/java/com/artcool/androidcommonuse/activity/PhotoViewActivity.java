package com.artcool.androidcommonuse.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Toast;

import com.artcool.androidcommonuse.R;
import com.artcool.androidcommonuse.activity.base.BaseActivity;
import com.artcool.androidcommonuse.adapter.PhotoPagerAdapter;
import com.artcool.commonusesdk.imageloader.ImageLoaderManager;
import com.artcool.commonusesdk.utils.ImageUtils;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;

/**
 * 浏览大图页面
 *
 * @author wuyibin
 * @date 2019/7/29
 */
public class PhotoViewActivity extends BaseActivity {

    private ViewPager mIndicatorViewPage;
    private AppCompatTextView mNumber;
    private AppCompatTextView mSave;

    private ArrayList<String> mPhotoList = new ArrayList<>();
    private PhotoPagerAdapter mAdapter;

    private int currentPosition;

    @Override
    protected void initListening() {
        mIndicatorViewPage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                currentPosition = i;
                mNumber.setText((currentPosition + 1) + "/" + mPhotoList.size());
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = ImageLoaderManager.getInstance(PhotoViewActivity.this).saveUrlAsBitmap(mPhotoList.get(currentPosition));
                if (ImageUtils.saveImageToAlbum(PhotoViewActivity.this,bitmap)) {
                    Toast.makeText(PhotoViewActivity.this,"save Success!",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(PhotoViewActivity.this,"save Failed!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void initData() {
        mPhotoList.add("http://p3.pstatp.com/large/pgc-image/3cd01c2d17f24f1f9c41be924a542599");
        mPhotoList.add("http://p1.pstatp.com/large/pgc-image/9910b81c2e484c95955ddf94597603f8");
        mPhotoList.add("http://p1.pstatp.com/large/pgc-image/RXVMBXF5YtwNYY");
        mPhotoList.add("http://p1.pstatp.com/large/pgc-image/RXVTNPH38eyFKw");
        mPhotoList.add("http://p1.pstatp.com/large/pgc-image/807422a9469842298810cb7980ceffe8");
        Logger.d(mPhotoList);
        mAdapter = new PhotoPagerAdapter(this, mPhotoList);
        mIndicatorViewPage.setAdapter(mAdapter);

        mNumber.setText((currentPosition + 1) + "/" + mPhotoList.size());
    }

    @Override
    protected void initView() {
        setContentView(R.layout.activity_photo_view);
        mIndicatorViewPage = findViewById(R.id.indicator_view_page);
        mNumber = findViewById(R.id.photo_view_number);
        mSave = findViewById(R.id.photo_view_save);

    }

    @Override
    protected void initVariable() {

    }
}
