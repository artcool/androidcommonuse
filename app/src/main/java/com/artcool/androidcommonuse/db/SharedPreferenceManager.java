package com.artcool.androidcommonuse.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.artcool.androidcommonuse.application.CommonUseApplication;

/**
 * 配置文件管理工具类
 * @author wuyibin
 * @date 2018/11/30
 */
public class SharedPreferenceManager {

    private static SharedPreferences sp = null;
    private static SharedPreferenceManager manager = null;
    private static Editor editor = null;

    /**
     * sp文件名
     */
    private static final String SHARE_PREFERENCE_NAME = "acu.sp";

    public static final String LAST_UPDATE_PRODUCT = "last_update_product";
    public static final String VIDEO_PLAY_SETTING = "video_play_setting";
    public static final String IS_SHOW_GUIDE = "is_show_guide";

    private SharedPreferenceManager() {
        sp = CommonUseApplication.getInstance().getSharedPreferences(SHARE_PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    public static SharedPreferenceManager getInstance() {
        if (manager == null || sp == null || editor == null) {
            manager = new SharedPreferenceManager();
        }
        return manager;
    }

    public void putInt(String key, int value) {
        editor.putInt(key,value);
        editor.commit();
    }

    public int getInt(String key,int defaultValue) {
        return sp.getInt(key, defaultValue);
    }

    public void putLong(String key,long value) {
        editor.putLong(key, value);
        editor.commit();
    }

    public long getLong(String key,long defaultValue) {
        return sp.getLong(key, defaultValue);
    }

    public void putString(String key,String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getString(String key,String defaultValue) {
        return sp.getString(key, defaultValue);
    }

    public void putBoolean(String key,boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key,boolean defaultValue) {
        return sp.getBoolean(key, defaultValue);
    }

    public void putFloat(String key,float value) {
        editor.putFloat(key, value);
        editor.commit();
    }

    public float getFloat(String key,float defaultValue) {
        return sp.getFloat(key, defaultValue);
    }

    /**
     * 判断配置文件中的key是否存在
     * @param key
     * @return
     */
    public boolean isKeyExist(String key) {
        return sp.contains(key);
    }

    public void remove(String key) {
        editor.remove(key);
        editor.commit();
    }
}
