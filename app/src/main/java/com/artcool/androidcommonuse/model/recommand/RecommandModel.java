package com.artcool.androidcommonuse.model.recommand;

import java.util.ArrayList;

/**
 *
 * @author wuyibin
 * @date 2018/10/31
 */
public class RecommandModel extends BaseRecommandModel {
    public ArrayList<RecommandBodyValue> list;
    public RecommandHeadValue head;
}
