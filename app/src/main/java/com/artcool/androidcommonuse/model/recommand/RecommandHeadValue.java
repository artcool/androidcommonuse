package com.artcool.androidcommonuse.model.recommand;

import com.artcool.androidcommonuse.model.base.BaseModel;

import java.util.ArrayList;

/**
 *
 * @author wuyibin
 * @date 2018/10/31
 */
public class RecommandHeadValue extends BaseModel {

    public ArrayList<String> ads;
    public ArrayList<String> middle;
    public ArrayList<RecommandFooterValue> footer;

}
