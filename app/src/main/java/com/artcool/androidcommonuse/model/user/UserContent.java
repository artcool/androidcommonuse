package com.artcool.androidcommonuse.model.user;

import com.artcool.androidcommonuse.model.base.BaseModel;

/**
 *
 * @author wuyibin
 * @date 2019/7/23
 */
public class UserContent extends BaseModel {

    public String userId;
    public String photoUrl;
    public String username;
    public String tick;
    public String phoneNumber;
    public String address;
}
