package com.artcool.androidcommonuse.model.recommand;

import com.artcool.androidcommonuse.model.base.BaseModel;

/**
 *  首页数据模型父类
 * @author wuyibin
 * @date 2018/10/31
 */
public class BaseRecommandModel extends BaseModel {
    public String ecode;
    public String emsg;
    public RecommandModel data;
}
