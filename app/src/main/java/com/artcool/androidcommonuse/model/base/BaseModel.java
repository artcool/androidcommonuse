package com.artcool.androidcommonuse.model.base;

import java.io.Serializable;

/**
 * 数据模型基类
 * @author wuyibin
 * @date 2018/10/31
 */
public class BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;
}
