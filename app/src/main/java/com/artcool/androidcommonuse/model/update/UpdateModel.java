package com.artcool.androidcommonuse.model.update;

import com.artcool.androidcommonuse.model.base.BaseModel;

/**
 *
 * @author wuyibin
 * @date 2019/7/15
 */
public class UpdateModel extends BaseModel {

    public int code;
    public String msg;
    public UpdateInfo data;
}
