package com.artcool.androidcommonuse.model.recommand;

import com.artcool.androidcommonuse.model.base.BaseModel;

import java.util.ArrayList;

/**
 *
 * @author wuyibin
 * @date 2018/10/31
 */
public class RecommandBodyValue extends BaseModel {
    public int type;
    public String logo;
    public String title;
    public String info;
    public String price;
    public String text;
    public String site;
    public String from;
    public String zan;
    public ArrayList<String> url;

    //视频专用
    public String thumb;
    public String resource;
    public String resourceID;
    public String adid;
}
