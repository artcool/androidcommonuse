package com.artcool.androidcommonuse.model.user;

import com.artcool.androidcommonuse.model.base.BaseModel;

/**
 *
 * @author wuyibin
 * @date 2019/7/23
 */
public class User extends BaseModel {

    public int code;
    public String msg;
    public UserContent data;
}
