package com.artcool.androidcommonuse.model.recommand;

import com.artcool.androidcommonuse.model.base.BaseModel;

/**
 *
 * @author wuyibin
 * @date 2018/10/31
 */
public class RecommandFooterValue extends BaseModel {
    public String title;
    public String info;
    public String from;
    public String imageOne;
    public String imageTwo;
    public String destationUrl;
}
