package com.artcool.androidcommonuse.manager;

import com.artcool.androidcommonuse.model.user.User;

/**
 *  用户信息管理类
 * @author wuyibin
 * @date 2019/7/23
 */
public class UserManager {

    private static UserManager userManager = null;

    private User user = null;

    public static UserManager getInstance() {
        if (userManager == null) {
            synchronized (UserManager.class) {
                if (userManager == null) {
                    userManager = new UserManager();
                }
                return userManager;
            }
        } else {
            return userManager;
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * 判断用户是否登录
     * @return
     */
    public boolean isLogined() {
        return user == null ? false : true;
    }

    /**
     * 删除用户
     */
    public void removeUser() {
        this.user = null;
    }
}
