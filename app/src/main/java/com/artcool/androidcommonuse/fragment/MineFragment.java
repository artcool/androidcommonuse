package com.artcool.androidcommonuse.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.artcool.androidcommonuse.R;
import com.artcool.androidcommonuse.activity.LoginActivity;
import com.artcool.androidcommonuse.activity.MainActivity;
import com.artcool.androidcommonuse.activity.PhotoViewActivity;
import com.artcool.androidcommonuse.activity.VideoSettingActivity;
import com.artcool.androidcommonuse.fragment.base.BaseFragment;
import com.artcool.androidcommonuse.model.update.UpdateModel;
import com.artcool.androidcommonuse.network.http.RequestCenter;
import com.artcool.androidcommonuse.widget.RoundImageTextView;
import com.artcool.commonusesdk.okhttp.listener.DisposeDataListener;
import com.artcool.commonusesdk.utils.Utils;
import com.artcool.commonusesdk.widget.toast.ACUToast;
import com.artcool.commonusesdk.zxing.util.Util;

/**
 *  个人中心
 * @author wuyibin
 * @date 2018/10/25
 */
public class MineFragment extends BaseFragment implements View.OnClickListener {

    private View mMineView;
    private RoundImageTextView mMineImage;
    private Button mMineLogin;
    private RelativeLayout mVideoSetting;
    private RelativeLayout mMineShare;
    private RelativeLayout mMineQRcode;
    private RelativeLayout mVersionUpdate;

    private LoginBroadcastReceiver mReceiver = new LoginBroadcastReceiver();
    private AppCompatImageView mImageQRCode;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerBroadcast();
    }

    private void registerBroadcast() {
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mReceiver,new IntentFilter(LoginActivity.LOGIN_ACTION));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        mMineView = inflater.inflate(R.layout.fragment_mine_layout, container, false);
        initView();
        return mMineView;
    }

    private void initView() {
        mMineImage = mMineView.findViewById(R.id.mine_image);
        mMineLogin = mMineView.findViewById(R.id.mine_login);
        mVideoSetting = mMineView.findViewById(R.id.mine_video_setting);
        mMineShare = mMineView.findViewById(R.id.mine_share);
        mMineQRcode = mMineView.findViewById(R.id.mine_qrcode);
        mVersionUpdate = mMineView.findViewById(R.id.mine_version_update);
        mImageQRCode = mMineView.findViewById(R.id.iv_mine_qrcode);
        mMineImage.setOnClickListener(this);
        mMineLogin.setOnClickListener(this);
        mVideoSetting.setOnClickListener(this);
        mMineShare.setOnClickListener(this);
        mMineQRcode.setOnClickListener(this);
        mVersionUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mine_image:
                //用户更换头像
//                ACUToast.error(mContext,"change image",Toast.LENGTH_SHORT,true).show();
                startActivity(new Intent(mContext, PhotoViewActivity.class));
                break;
            case R.id.mine_login:
                //登录
                startActivity(new Intent(mContext, LoginActivity.class));
                break;
            case R.id.mine_video_setting:
                //视频播放设置
//                ACUToast.info(mContext,"video--info",Toast.LENGTH_SHORT,true).show();
                startActivity(new Intent(mContext,VideoSettingActivity.class));
                break;
            case R.id.mine_share:
                //分享
                ACUToast.warning(mContext,"share--warning",Toast.LENGTH_SHORT,true).show();
                break;
            case R.id.mine_qrcode:
                //生成二维码
                mImageQRCode.setImageBitmap(Utils.createQRCode(Utils.dip2px(mContext,200),Utils.dip2px(mContext,200),"abc"));
                break;
            case R.id.mine_version_update:
                //版本更新
                checkVersion();
                break;
            default:
                break;
        }
    }

    /**
     * 应用版本更新
     */
    private void checkVersion() {
        RequestCenter.checkVersion(new DisposeDataListener() {
            @Override
            public void onSuccess(Object responseObj) {
                UpdateModel updateModel = (UpdateModel) responseObj;
                if (Util.getVersionCode(mContext) < updateModel.data.currentVersion) {
                    //说明有新版本，处理下载更新逻辑

                    /*
                    开启下载更新服务
                    Intent intent = new Intent(mContext, UpdateService.class);
                            mContext.startService(intent);
                     */
                } else {
                    //提示当前已经是最新版本等处理
                }
            }

            @Override
            public void onFailure(Object reasonObj) {

            }
        });
    }

    private class LoginBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            //刷新UI操作
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterBroadcast();
    }

    private void unregisterBroadcast() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mReceiver);
    }
}
