package com.artcool.androidcommonuse.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.artcool.androidcommonuse.R;
import com.artcool.androidcommonuse.fragment.base.BaseFragment;
import com.artcool.androidcommonuse.model.recommand.BaseRecommandModel;
import com.artcool.androidcommonuse.network.http.RequestCenter;
import com.artcool.commonusesdk.okhttp.listener.DisposeDataListener;
import com.artcool.commonusesdk.zxing.activity.CaptureActivity;
import com.orhanobut.logger.Logger;

/**
 * @author wuyibin
 * @date 2018/10/25
 */
public class HomeFragment extends BaseFragment {

    private static final int CAMERA_PERMISSION = 2;
    private static final int RESULT_QRCODE = 0x01;

    private View mHomeView;
    private ListView mListView;
    private BaseRecommandModel mRecommonData;
    private Button mScan;

    public HomeFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    private void initData() {
        RequestCenter.requestRecommandData(new DisposeDataListener() {
            @Override
            public void onSuccess(Object responseObj) {
                mRecommonData = (BaseRecommandModel) responseObj;
                showSuccessView();
            }

            @Override
            public void onFailure(Object reasonObj) {
                Logger.i(reasonObj.toString());
            }
        });
    }

    private void showSuccessView() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        mHomeView = inflater.inflate(R.layout.fragment_home_layout, container, false);
        initView();
        initListening();
        return mHomeView;
    }

    private void initListening() {
        mScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(mContext,
                            new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION);
                } else {
                    Intent intent = new Intent(mContext, CaptureActivity.class);
                    startActivityForResult(intent,RESULT_QRCODE);
                }

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(mContext, CaptureActivity.class);
                    startActivityForResult(intent,RESULT_QRCODE);
                } else {
                    Logger.e("Please grant camera permission to use the QR Scanner");
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.i(requestCode + ", " + resultCode + ", " + data.toString());
        if (requestCode == RESULT_QRCODE) {
           if (resultCode == Activity.RESULT_OK) {
               String result = data.getStringExtra("SCAN_RESULT");
               //TODO 处理二维码扫描回调结果
               Logger.i(result);
           }
        }
    }

    private void initView() {
        mScan = mHomeView.findViewById(R.id.btn_scan);
        mListView = mHomeView.findViewById(R.id.list_view);
    }

}
