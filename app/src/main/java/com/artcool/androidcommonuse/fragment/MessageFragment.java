package com.artcool.androidcommonuse.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.artcool.androidcommonuse.R;
import com.artcool.androidcommonuse.fragment.base.BaseFragment;

/**
 * Created by wuyibin on 2018/10/25.
 */
public class MessageFragment extends BaseFragment {

    private View mMessageView;

    public MessageFragment(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        mMessageView = inflater.inflate(R.layout.fragment_message_layout, container, false);
        return mMessageView;
    }
}
