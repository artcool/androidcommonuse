package com.artcool.androidcommonuse.network.http;

import com.artcool.androidcommonuse.model.recommand.BaseRecommandModel;
import com.artcool.androidcommonuse.model.update.UpdateModel;
import com.artcool.androidcommonuse.model.user.User;
import com.artcool.commonusesdk.okhttp.OkHttpClientManager;
import com.artcool.commonusesdk.okhttp.listener.DisposeDataHandle;
import com.artcool.commonusesdk.okhttp.listener.DisposeDataListener;
import com.artcool.commonusesdk.okhttp.request.CommonRequest;
import com.artcool.commonusesdk.okhttp.request.RequestParams;
import com.orhanobut.logger.Logger;


/**
 * 封装网络请求
 * @author wuyibin
 * @date 2018/10/31
 */
public class RequestCenter {

    public static void postRequest(String url, RequestParams params, DisposeDataListener listener,Class<?> clazz) {
        OkHttpClientManager.get(CommonRequest.createGetRequest(url, params),new DisposeDataHandle(listener,clazz));
    }

    public static void requestRecommandData(DisposeDataListener listener) {
        Logger.i(HttpConstants.HOME_RECOMMAND);
        RequestCenter.postRequest(HttpConstants.HOME_RECOMMAND,null,listener,BaseRecommandModel.class);
    }

    /**
     * 应用版本号更新请求
     * @param listener
     */
    public static void checkVersion(DisposeDataListener listener) {
        RequestCenter.postRequest(HttpConstants.CHECK_UPDATE,null,listener, UpdateModel.class);
    }

    public static void login(String username,String pwd,DisposeDataListener listener) {
        RequestParams params = new RequestParams();
        params.put("username",username);
        params.put("password",pwd);
        RequestCenter.postRequest(HttpConstants.LOGIN,params,listener, User.class);
    }

}
