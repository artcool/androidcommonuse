package com.artcool.commonusesdk.module;

import java.util.ArrayList;

/**
 * 视频广告
 * @author wuyb
 */
public class VideoInstance {
    public ArrayList<VideoValue> values;
    public String version;
    public String type;
}
