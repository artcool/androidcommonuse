package com.artcool.commonusesdk.module;

import com.artcool.commonusesdk.module.monitor.Monitor;
import com.artcool.commonusesdk.module.monitor.emevent.EMEvent;

import java.util.ArrayList;

/**
 *
 * @author wuyibin
 * @date 2018/11/20
 */
public class VideoValue {
    public String resourceID;
    public String adid;
    public String resource;
    public String thumb;
    public ArrayList<Monitor> startMonitor;
    public ArrayList<Monitor> middleMonitor;
    public ArrayList<Monitor> endMonitor;
    public String clickUrl;
    public ArrayList<Monitor> clickMonitor;
    public EMEvent event;
    public String type;
}
