package com.artcool.commonusesdk.imageloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

import com.artcool.commonusesdk.R;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * 初始化UniversalImageLoader，并用来加载网络图片
 *
 * @author wuyibin
 * @date 2018/10/26
 */
public class ImageLoaderManager {

    /**
     * 设置UIL最多可以有4条线程
     */
    private static final int THREAD_COUNT = 4;
    /**
     * 标明图片加载的优先级
     */
    private static final int PRORITY = 2;
    /**
     * 标明可以最多缓存多少张图片
     */
    private static final int DISK_CACHE_SIZE = 50 * 1024;
    /**
     * 连接的超时时间
     */
    private static final int CONNECTION_TIME_OUT = 5 * 1000;
    /**
     * 读取的超时时间
     */
    private static final int READ_TIME_OUT = 30 * 1000;

    private static ImageLoaderManager mInstance = null;
    private static ImageLoader mLoader = null;

    /**
     * 在私有构造函数对ImageLoader进行初始化
     *
     * @param context
     */
    private ImageLoaderManager(Context context) {
        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder(context)
                //配置图片下载线程的最大数量
                .threadPoolSize(THREAD_COUNT)
                //配置优先级
                .threadPriority(Thread.NORM_PRIORITY - PRORITY)
                //防止缓存多套尺寸图片
                .denyCacheImageMultipleSizesInMemory()
                //使用弱引用内存缓存
                .memoryCache(new WeakMemoryCache())
                //分配硬盘缓存大小
                .diskCacheSize(DISK_CACHE_SIZE)
                //使用MD5命名文件
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                //图片下载顺序
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                //默认的图片加载options
                .defaultDisplayImageOptions(getDefaultOptions())
                //设置图片下载器
                .imageDownloader(new BaseImageDownloader(context, CONNECTION_TIME_OUT, READ_TIME_OUT))
                //debug环境下会输出日志
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(configuration);
        mLoader = ImageLoader.getInstance();
    }

    public static ImageLoaderManager getInstance(Context context) {
        if (mInstance == null) {
            synchronized (ImageLoaderManager.class) {
                if (mInstance == null) {
                    mInstance = new ImageLoaderManager(context);
                }
            }
        }
        return mInstance;
    }

    /**
     * 分配一个默认的options
     *
     * @return
     */
    private DisplayImageOptions getDefaultOptions() {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                //处理图片地址为空的时候，用默认图片代替
                .showImageForEmptyUri(R.mipmap.cusdk_img_error)
                //图片下载失败的时候显示的图片
                .showImageOnFail(R.mipmap.cusdk_img_error)
                //设置图片可以缓存在内存
                .cacheInMemory(true)
                //设置图片可以缓存在硬盘
                .cacheOnDisk(true)
                //是否考虑JEPG图像的EXIF参数(旋转，翻转)
                .considerExifParams(true)
                //设置图片加载以何种的编码方式显示
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                //使用的图片解码类型
                .bitmapConfig(Bitmap.Config.RGB_565)
                //图片解码配置
                .decodingOptions(new BitmapFactory.Options())
                .build();
        return options;
    }

    /**
     * 加载图片api
     *
     * @param imageView
     * @param url
     * @param options
     * @param listener
     */
    public void displayImage(ImageView imageView, String url, DisplayImageOptions options, ImageLoadingListener listener) {
        if (mLoader != null) {
            mLoader.displayImage(url, imageView, options, listener);
        }
    }

    public void displayImage(ImageView imageView, String url, ImageLoadingListener listener) {
        if (mLoader != null) {
            mLoader.displayImage(url, imageView, listener);
        }
    }

    public void displayImage(ImageView imageView, String url) {
        displayImage(imageView, url, null);
    }

    public void displayImageFromDrawable(ImageView imageView,@DrawableRes int imageId) {
        displayImage(imageView,"drawable://" + imageId,null);
    }

    public Bitmap saveUrlAsBitmap(String url) {
        return mLoader.loadImageSync(url,getDefaultOptions());
    }

    public DisplayImageOptions getOptionsWithoutCache() {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                //是否考虑JPEG图像EXIF参数（旋转，翻转）
                .considerExifParams(true)
                //设置图片以如何的编码方式显示
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                //设置图片的解码类型
                .bitmapConfig(Bitmap.Config.RGB_565)
                //设置图片的解码配置
                .decodingOptions(new BitmapFactory.Options())
                //设置图片在下载前是否重置，复位
                .resetViewBeforeLoading(true)
                .displayer(new FadeInBitmapDisplayer(400))
                .build();
        return options;
    }

}
