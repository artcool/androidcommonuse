package com.artcool.commonusesdk.widget.videoview;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.artcool.commonusesdk.R;
import com.artcool.commonusesdk.constant.SDKConstant;
import com.artcool.commonusesdk.core.Parameters;
import com.artcool.commonusesdk.utils.Utils;
import com.orhanobut.logger.Logger;

import java.io.IOException;

/**
 * 自定义视频播放器view
 *
 * @author wuyibin
 * @date 2018/11/15
 */
public class CustomVideoView extends RelativeLayout implements View.OnClickListener,
        MediaPlayer.OnPreparedListener, MediaPlayer.OnInfoListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener, TextureView.SurfaceTextureListener {

    /**
     * Constant
     */
    private static final String TAG = "MraidVideoView";
    private static final int TIME_MSG = 0x01;
    private static final int TIME_INVAL = 1000;
    private static final int STATE_ERROR = -1;
    private static final int STATE_IDLE = 0;
    private static final int STATE_PLAYING = 1;
    private static final int STATE_PAUSING = 2;
    private static final int LOAD_TOTAL_COUNT = 3;
    /**
     * UI
     */
    private ViewGroup mParentContainer;
    private AudioManager audioManager;
    private RelativeLayout mPlayerView;
    private TextureView mVideoView;
    private ImageView mFraming;
    private ImageView mFullView;
    private Button mPlayBuntton;
    private MediaPlayer mediaPlayer;
    private Surface videoSurface;

    /**
     * Data
     */
    private String mUrl;
    private String mFrameURI;
    private boolean isMute;
    private int mScreenWidth, mDestationHeight;

    private ImageView mLoadingBar;

    private ScreenEventReceiver mScreenReceiver;

    /**
     * Status状态保护
     */
    private boolean canPlay = true;
    private boolean mIsRealPause;
    private boolean mIsComplete;
    private int mCurrentCount;
    private int playerState = STATE_IDLE;

    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case TIME_MSG:
                    if (isPlaying()) {
                        videoPlayerListener.onBufferUpdate(getCurrentPosition());
                        sendEmptyMessageDelayed(TIME_MSG,TIME_INVAL);
                    }
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * 获取当前播放的位置
     * @return
     */
    public int getCurrentPosition() {
        if (mediaPlayer != null) {
            return mediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    public int getDuration() {
        if (mediaPlayer != null) {
            return mediaPlayer.getDuration();
        }
        return 0;
    }


    public CustomVideoView(Context context, ViewGroup parentContainer) {
        super(context);
        mParentContainer = parentContainer;
        audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        initData();
        initView();
        registerBroadcastReceiver();
    }



    public CustomVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * 是否处于播放状态
     * @return
     */
    public boolean isPlaying() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            return true;
        }
        return false;
    }

    /**
     * 初始化视频播放器
     *
     * @return
     */
    private MediaPlayer createMediaPlayer() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.reset();
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnInfoListener(this);
        mediaPlayer.setOnErrorListener(this);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        if (videoSurface != null && videoSurface.isValid()) {
            mediaPlayer.setSurface(videoSurface);
        } else {
            stop();
        }
        return mediaPlayer;

    }

    private void stop() {
        Logger.d("视频播放器停止播放");
        if (this.mediaPlayer != null) {
            this.mediaPlayer.reset();
            this.mediaPlayer.setOnSeekCompleteListener(null);
            this.mediaPlayer.stop();
            this.mediaPlayer.release();
            this.mediaPlayer = null;
        }

        mHandler.removeCallbacksAndMessages(null);
        setCurrentPlayState(STATE_IDLE);

        if (mCurrentCount >= LOAD_TOTAL_COUNT) {
           mCurrentCount += 1;
           load();
        } else {
            //显示暂停状态
            showPauseView(false);
        }

    }

    private void initView() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        mPlayerView = (RelativeLayout) inflater.inflate(R.layout.custom_video_player, this);
        mVideoView = mPlayerView.findViewById(R.id.custom_video_texture);
        mVideoView.setOnClickListener(this);
        mVideoView.setKeepScreenOn(true);
        mVideoView.setSurfaceTextureListener(this);
        //初始化，小屏布局状态
        initSmallLayoutMode();

    }

    /**
     * 小屏状态
     */
    private void initSmallLayoutMode() {
        LayoutParams params = new LayoutParams(mScreenWidth, mDestationHeight);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mPlayerView.setLayoutParams(params);

        mFraming = mPlayerView.findViewById(R.id.framing_view);
        mFullView = mPlayerView.findViewById(R.id.full_view);
        mPlayBuntton = mPlayerView.findViewById(R.id.video_small_play_btn);
        mLoadingBar = mPlayerView.findViewById(R.id.loading_bar);

        mPlayBuntton.setOnClickListener(this);
        mFullView.setOnClickListener(this);

    }

    /**
     * 设置异常显示图
     *
     * @param resID
     */
    public void setFrameResource(int resID) {
        mFraming.setImageResource(resID);
    }

    /**
     * 设置显示异常图
     */
    public void setFrameVisible() {
        mFraming.setVisibility(View.VISIBLE);
    }

    /**
     * 设置不显示异常图
     */
    public void setFrameGone() {
        mFraming.setVisibility(View.GONE);
    }

    /**
     * 设置全屏或者非全屏按钮图
     *
     * @param resID
     */
    public void setFullView(int resID) {
        mFullView.setImageResource(resID);
    }

    /**
     * 设置播放暂停按钮图
     *
     * @param resID
     */
    public void setPlayButton(int resID) {
        mPlayBuntton.setBackgroundResource(resID);
    }


    private void initData() {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(dm);
        mScreenWidth = dm.widthPixels;
        mDestationHeight = (int) (mScreenWidth * SDKConstant.VIDEO_HEIGHT_PERCENT);
    }

    public boolean isFrameHidden() {
        return mFraming.getVisibility() == View.VISIBLE ? false : true;
    }

    @Override
    public void onClick(View v) {
        if (v == mPlayBuntton) {
            if (this.playerState == STATE_PAUSING) {
                if (Utils.getVisiblePercent(mParentContainer)
                        > SDKConstant.VIDEO_SCREEN_PERCENT) {
                    resume();
                    this.videoPlayerListener.onClickPlay();
                }
            } else {
                load();
            }
        } else if (v == mFullView) {
            this.videoPlayerListener.onClickFullScreenButton();
        } else if (v == mVideoView) {
            this.videoPlayerListener.onClickVideo();
        }
    }

    public void resume() {
        if (playerState != STATE_PAUSING) {
            return;
        }
        Logger.d("播放器处于resume状态");
        if (!isPlaying()) {
            entryResumeState();
            mediaPlayer.setOnSeekCompleteListener(null);
            mediaPlayer.start();
            mHandler.sendEmptyMessage(TIME_MSG);
            showPauseView(true);
        } else {
            showPauseView(false);
        }

    }

    /**
     * 进入resume状态
     */
    private void entryResumeState() {
        canPlay = true;
        setCurrentPlayState(STATE_PLAYING);
        setIsRealPause(false);
        setIsComplete(false);
    }

    /**
     * 是否播放完成
     * @param isComplete
     */
    public void setIsComplete(boolean isComplete) {
        this.mIsComplete = isComplete;
    }

    /**
     * 是否是真正处于暂停状态
     * @param isRealPause
     */
    public void setIsRealPause(boolean isRealPause) {
        this.mIsRealPause = isRealPause;
    }

    /**
     * 暂停状态的view处理
     * @param show
     */
    private void showPauseView(boolean show) {
        mFullView.setVisibility(show ? View.VISIBLE : View.GONE);
        mPlayBuntton.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoadingBar.clearAnimation();
        mLoadingBar.setVisibility(View.GONE);
        if (!show) {
            mFraming.setVisibility(View.VISIBLE);
            loadFrameImage();
        } else {
            mFraming.setVisibility(View.GONE);
        }
    }

    /**
     * 异步加载定帧图
     */
    private void loadFrameImage() {
        if (frameImageLoaderListener != null) {
            frameImageLoaderListener.onStartFrameLoad(mFrameURI, new ImageLoaderListener() {
                @Override
                public void onLoadingComplete(Bitmap loadedImage) {
                    if (loadedImage != null) {
                        mFraming.setScaleType(ImageView.ScaleType.FIT_XY);
                        mFraming.setImageBitmap(loadedImage);
                    } else {
                        mFraming.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        mFraming.setImageResource(R.mipmap.video_img_error);
                    }
                }
            });
        }

    }

    /**
     * 加载状态下的view处理
     */
    private void showLoadingView() {
        mFullView.setVisibility(View.GONE);
        mLoadingBar.setVisibility(View.VISIBLE);
        AnimationDrawable anim = (AnimationDrawable) mLoadingBar.getBackground();
        anim.start();
        mPlayBuntton.setVisibility(View.GONE);
        mFraming.setVisibility(View.GONE);
        loadFrameImage();
    }

    /**
     * 播放状态下的view处理
     */
    private void showPlayView() {
        mLoadingBar.clearAnimation();
        mLoadingBar.setVisibility(View.GONE);
        mPlayBuntton.setVisibility(View.GONE);
        mFraming.setVisibility(View.GONE);
    }

    public void isShowFullBtn(boolean isShow) {
        mFullView.setImageResource(isShow ? R.mipmap.mini : R.mipmap.full_mini);
        mFullView.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    public boolean isRealPause() {
        return mIsRealPause;
    }

    public boolean isComplete() {
        return mIsComplete;
    }

    /**
     * 设置视频播放的地址
     * @param url
     */
    public void setDataSource(String url) {
        this.mUrl = url;
    }

    public void setFrameURI(String url) {
        this.mFrameURI = url;
    }

    /**
     * 加载视频
     */
    public void load() {
        if (playerState != STATE_IDLE) {
            return;
        }
        Logger.i("播放视频的url地址：" + mUrl);
        showLoadingView();
        try {
            setCurrentPlayState(STATE_IDLE);
            checkMediaPlayer();
            mute(true);
            mediaPlayer.setDataSource(mUrl);
            //开始异步加载
            mediaPlayer.prepareAsync();
        } catch (IOException e) {
            Logger.i("加载异常信息：" + e.getMessage());
            stop();
        }
    }

    /**
     * 是否静音
     * 默认静音
     * @param mute
     */
    public void mute(boolean mute) {
        Logger.i("当前是否处于静音状态 : " + mute);
        isMute = mute;
        if (mediaPlayer != null && audioManager != null) {
            float volume = isMute ? 0.0f : 1.0f;
            mediaPlayer.setVolume(volume,volume);
        }
    }

    /**
     * 设置当前播放状态
     * @param stateIdle
     */
    private void setCurrentPlayState(int stateIdle) {
        this.playerState = stateIdle;
    }

    /**
     * 检查播放器对象是否被创建
     */
    private synchronized void checkMediaPlayer() {
        if (mediaPlayer == null) {
            //每次都创建一个播放器
            mediaPlayer = createMediaPlayer();
        }
    }

    /**
     * 监听锁屏事件的广播接收器
     */
    private class ScreenEventReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            //主动锁屏时 pause, 主动解锁屏幕时，resume
            switch (intent.getAction()) {
                case Intent.ACTION_USER_PRESENT:
                    if (playerState == STATE_PAUSING) {
                        if (mIsRealPause) {
                            //手动点的暂停，回来后还暂停
                            pause();
                        } else {
                            decideCanPlay();
                        }
                    }
                    break;
                case Intent.ACTION_SCREEN_OFF:
                    if (playerState == STATE_PLAYING) {
                        pause();
                    }
                    break;
                    default:
                        break;
            }
        }
    }

    /**
     * 决定是否能播放
     */
    private void decideCanPlay() {
        if (Utils.getVisiblePercent(mParentContainer) > SDKConstant.VIDEO_SCREEN_PERCENT) {
            resume();
        } else {
            pause();
        }
    }

    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (visibility == VISIBLE && playerState == STATE_PAUSING) {
            if (isRealPause() || isComplete()) {
                pause();
            } else {
                decideCanPlay();
            }
        } else {
            pause();
        }
    }

    /**
     * 视频播放器暂停状态
     */
    public void pause() {
        if (this.playerState != STATE_PLAYING) {
            return;
        }

        Logger.i("播放器处于暂停状态");
        setCurrentPlayState(STATE_PAUSING);
        if (isPlaying()) {
            mediaPlayer.pause();
            if (!canPlay) {
                mediaPlayer.seekTo(0);
            }
        }
        showPauseView(false);
        mHandler.removeCallbacksAndMessages(null);
    }

    /**跳到指定点播放视频*/
    public void seekAndResume(int position) {
        if (mediaPlayer != null) {
            showPauseView(true);
            entryResumeState();
            mediaPlayer.seekTo(position);
            mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                @Override
                public void onSeekComplete(MediaPlayer mp) {
                    Logger.i( "跳到指定点播放视频");
                    mediaPlayer.start();
                    mHandler.sendEmptyMessage(TIME_MSG);
                }
            });
        }
    }

    /**
     * 跳到指定点暂停视频
     */
    public void seekAndPause(int position) {
        if (this.playerState != STATE_PLAYING) {
            return;
        }
        showPauseView(false);
        setCurrentPlayState(STATE_PAUSING);
        if (isPlaying()) {
            mediaPlayer.seekTo(position);
            mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                @Override
                public void onSeekComplete(MediaPlayer mp) {
                    Logger.i( "跳到指定点暂停视频");
                    mediaPlayer.pause();
                    mHandler.removeCallbacksAndMessages(null);
                }
            });
        }
    }

    public void destroy() {
        Logger.i("播放器销毁");
        if (mediaPlayer != null) {
            mediaPlayer.setOnSeekCompleteListener(null);
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        setCurrentPlayState(STATE_IDLE);
        mCurrentCount = 0;
        setIsComplete(false);
        setIsRealPause(false);
        unRegisterBroadcastReceiver();
        //release all message and runnable
        mHandler.removeCallbacksAndMessages(null);
        //除了播放和loading外其余任何状态都显示pause
        showPauseView(false);
    }

    /**
     * 全屏不显示暂停状态，后续可以整合，不必单独出一个方法
     */
    public void pauseForFullScreen() {
        if (playerState != STATE_PLAYING) {
            return;
        }
        Logger.i("全屏暂停状态");
        setCurrentPlayState(STATE_PAUSING);
        if (isPlaying()) {
            mediaPlayer.pause();
            if (!canPlay) {
                mediaPlayer.seekTo(0);
            }
        }
        mHandler.removeCallbacksAndMessages(null);
    }

    /**
     * 播放完成回到初始状态
     */
    private void playBack() {
        Logger.i("播放完成回到初始状态");
        setCurrentPlayState(STATE_PAUSING);
        mHandler.removeCallbacksAndMessages(null);
        if (mediaPlayer != null) {
            mediaPlayer.setOnSeekCompleteListener(null);
            mediaPlayer.seekTo(0);
            mediaPlayer.pause();
        }
        showPauseView(false);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Logger.i("video prepared");
        showPlayView();
        mediaPlayer = mp;
        if (mediaPlayer != null) {
            mediaPlayer.setOnBufferingUpdateListener(this);
            mCurrentCount = 0;
            if (videoPlayerListener != null) {
                videoPlayerListener.onVideoLoadSuccess();
            }
            if (Utils.canAutoPlay(getContext(),Parameters.getCurrentSetting()) && Utils.getVisiblePercent(mParentContainer) > SDKConstant.VIDEO_SCREEN_PERCENT) {
                setCurrentPlayState(STATE_PAUSING);
                resume();
            } else {
                setCurrentPlayState(STATE_PLAYING);
                pause();
            }
        }
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        return true;
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        this.playerState = STATE_ERROR;
        mediaPlayer = mp;
        if (mediaPlayer != null) {
            mediaPlayer.reset();
        }

        if (mCurrentCount >= LOAD_TOTAL_COUNT) {
            showPauseView(false);
            if (videoPlayerListener != null) {
                videoPlayerListener.onVideoLoadFailed();
            }
        }
        stop();
        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (videoPlayerListener != null) {
            videoPlayerListener.onVideoLoadComplete();
        }
        playBack();
        setIsComplete(true);
        setIsRealPause(true);
    }



    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {

    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        Logger.i("onSurfaceTextureAvailable");
        videoSurface = new Surface(surface);
        checkMediaPlayer();
        mediaPlayer.setSurface(videoSurface);
        load();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        Logger.i("onSurfaceTextureSizeChanged");
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    private void registerBroadcastReceiver() {
        if (mScreenReceiver == null) {
            mScreenReceiver = new ScreenEventReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            filter.addAction(Intent.ACTION_USER_PRESENT);
            getContext().registerReceiver(mScreenReceiver, filter);
        }
    }

    private void unRegisterBroadcastReceiver() {
        if (mScreenReceiver != null) {
            getContext().unregisterReceiver(mScreenReceiver);
        }
    }

    /**
     * 供slot层来实现具体点击逻辑，具体逻辑还会变
     * 如果对UI的点击没有具体监测的话可以不回调
     */
    public interface VideoPlayerListener {

         void onBufferUpdate(int time);

         void onClickFullScreenButton();

         void onClickVideo();

         void onClickBackButton();

         void onClickPlay();

         void onVideoLoadSuccess();

         void onVideoLoadFailed();

         void onVideoLoadComplete();

    }

    private VideoPlayerListener videoPlayerListener;

    public void setVideoPlayerListener(VideoPlayerListener listener) {
        this.videoPlayerListener = listener;
    }

    public interface FrameImageLoaderListener {
        void onStartFrameLoad(String url,ImageLoaderListener listener);
    }

    private FrameImageLoaderListener frameImageLoaderListener;

    public void setOnFrameImageLoaderListener(FrameImageLoaderListener listener){
        this.frameImageLoaderListener = listener;
    }

    public interface ImageLoaderListener {
        /**
         * 如果图片下载不成功，传null
         * @param loadedImage
         */
        void onLoadingComplete(Bitmap loadedImage);
    }
}
