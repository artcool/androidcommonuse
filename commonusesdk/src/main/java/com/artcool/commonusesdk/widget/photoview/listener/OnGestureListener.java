package com.artcool.commonusesdk.widget.photoview.listener;

/**
 *
 * @author wuyibin
 * @date 2018/10/29
 */
public interface OnGestureListener {
    void onDrag(float dx,float dy);

    void onFling(float startX,float startY,float velocityX,float velocityY);

    void onScale(float scaleFactor,float focusX,float focusY);
}
