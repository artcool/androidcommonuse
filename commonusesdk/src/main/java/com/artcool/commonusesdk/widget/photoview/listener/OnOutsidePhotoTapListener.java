package com.artcool.commonusesdk.widget.photoview.listener;

import android.widget.ImageView;

/**
 *
 * @author wuyibin
 * @date 2018/10/29
 */
public interface OnOutsidePhotoTapListener {

    void onOutsidePhotoTap(ImageView imageView);
}
