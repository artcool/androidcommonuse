package com.artcool.commonusesdk.widget.ninegrid;

import java.io.Serializable;

/**
 *  图片信息
 * @author wuyibin
 * @date 2018/10/30
 */
public class ImageInfo implements Serializable {
    //缩略图
    private String thumbnailUrl;
    //大图
    private String bigImageUrl;
    //图片高度
    private int imageViewHeight;
    //图片宽度
    private int imageViewWidth;
    //图片横坐标
    private int imageViewX;
    //图片纵坐标
    private int imageViewY;

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getBigImageUrl() {
        return bigImageUrl;
    }

    public void setBigImageUrl(String bigImageUrl) {
        this.bigImageUrl = bigImageUrl;
    }

    public int getImageViewHeight() {
        return imageViewHeight;
    }

    public void setImageViewHeight(int imageViewHeight) {
        this.imageViewHeight = imageViewHeight;
    }

    public int getImageViewWidth() {
        return imageViewWidth;
    }

    public void setImageViewWidth(int imageViewWidth) {
        this.imageViewWidth = imageViewWidth;
    }

    public int getImageViewX() {
        return imageViewX;
    }

    public void setImageViewX(int imageViewX) {
        this.imageViewX = imageViewX;
    }

    public int getImageViewY() {
        return imageViewY;
    }

    public void setImageViewY(int imageViewY) {
        this.imageViewY = imageViewY;
    }

    @Override
    public String toString() {
        return "ImageInfo{" +
                "thumbnailUrl='" + thumbnailUrl + '\'' +
                ", bigImageUrl='" + bigImageUrl + '\'' +
                ", imageViewHeight=" + imageViewHeight +
                ", imageViewWidth=" + imageViewWidth +
                ", imageViewX=" + imageViewX +
                ", imageViewY=" + imageViewY +
                '}';
    }
}
