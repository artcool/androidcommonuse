package com.artcool.commonusesdk.widget.photoview.listener;

import android.widget.ImageView;

/**
 *
 * @author wuyibin
 * @date 2018/10/29
 */
public interface OnPhotoTapListener {
    void onPhotoTap(ImageView view, float x, float y);
}
