package com.artcool.commonusesdk.widget.photoview;

import android.content.Context;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

import com.artcool.commonusesdk.widget.photoview.listener.OnGestureListener;

/**
 * 自定义手势监测
 *
 * @author wuyibin
 * @date 2018/10/29
 */
public class CustomGestureDetector {
    private static final int INVALID_POINTER_ID = -1;
    private final int mMinimumVelocity;
    private final int mTouchSlop;
    private final ScaleGestureDetector mDetector;
    private OnGestureListener mListener;
    private int mActivePointerId = INVALID_POINTER_ID;
    private VelocityTracker mVelocityTracker;
    private int mActivePointerIndex = 0;
    private float mLastTouchX;
    private float mLastTouchY;
    private boolean mIsDragging;

    CustomGestureDetector(Context context, OnGestureListener listener) {
        ViewConfiguration configuration = ViewConfiguration.get(context);
        mMinimumVelocity = configuration.getScaledMinimumFlingVelocity();
        mTouchSlop = configuration.getScaledTouchSlop();
        mListener = listener;

        ScaleGestureDetector.OnScaleGestureListener mScaleListener = new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
                float scaleFactor = scaleGestureDetector.getScaleFactor();
                if (Float.isNaN(scaleFactor) || Float.isInfinite(scaleFactor)) {
                    return false;
                }
                mListener.onScale(scaleFactor, scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY());
                return true;
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
                return false;
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {

            }
        };
        mDetector = new ScaleGestureDetector(context, mScaleListener);
    }

    public boolean onTouchEvent(MotionEvent event) {
        try {
            mDetector.onTouchEvent(event);
            return processTouchEvent(event);
        } catch (IllegalArgumentException e) {
            return true;
        }
    }

    private boolean processTouchEvent(MotionEvent event) {
        int action = event.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                mActivePointerId = event.getPointerId(0);
                mVelocityTracker = VelocityTracker.obtain();
                if (mVelocityTracker != null) {
                    mVelocityTracker.addMovement(event);
                }
                mLastTouchX = getActiveX(event);
                mLastTouchY = getActiveY(event);
                mIsDragging = false;
                break;
            case MotionEvent.ACTION_MOVE:
                float x = getActiveX(event);
                float y = getActiveY(event);
                float dx = x - mLastTouchX, dy = y - mLastTouchY;
                if (!mIsDragging) {
                    mIsDragging = Math.sqrt((dx * dx) + (dy * dy)) >= mTouchSlop;
                }
                if (mIsDragging) {
                    mListener.onDrag(dx, dy);
                    mLastTouchX = x;
                    mLastTouchY = y;
                    if (null != mVelocityTracker) {
                        mVelocityTracker.addMovement(event);
                    }
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                mActivePointerId = INVALID_POINTER_ID;
                if (null != mVelocityTracker) {
                    mVelocityTracker.recycle();
                    mVelocityTracker = null;
                }
                break;
            case MotionEvent.ACTION_UP:
                mActivePointerId = INVALID_POINTER_ID;
                if (mIsDragging) {
                    if (null != mVelocityTracker) {
                        mLastTouchX = getActiveX(event);
                        mLastTouchY = getActiveY(event);
                        float vX = mVelocityTracker.getXVelocity(), vY = mVelocityTracker.getXVelocity();
                        if (Math.max(Math.abs(vX), Math.abs(vY)) >= mMinimumVelocity) {
                            mListener.onFling(mLastTouchX, mLastTouchY, -vX, -vY);
                        }
                    }
                }
                if (null != mVelocityTracker) {
                    mVelocityTracker.recycle();
                    mVelocityTracker = null;
                }
                break;
            case MotionEvent.ACTION_POINTER_UP:
                int pointerIndex = Util.getPointerIndex(event.getAction());
                int pointerId = event.getPointerId(pointerIndex);
                if (pointerId == mActivePointerId) {
                    int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mActivePointerId = event.getPointerId(newPointerIndex);
                    mLastTouchX = event.getX(newPointerIndex);
                    mLastTouchY = event.getY(newPointerIndex);
                }
                break;
            default:
                break;
        }
        mActivePointerIndex = event.findPointerIndex(mActivePointerId != INVALID_POINTER_ID ? mActivePointerId : 0);
        return true;
    }

    private float getActiveY(MotionEvent event) {
        try {
            return event.getY(mActivePointerIndex);
        } catch (Exception e) {
            return event.getY();
        }
    }

    private float getActiveX(MotionEvent event) {
        try {
            return event.getX(mActivePointerIndex);
        } catch (Exception e) {
            return event.getX();
        }
    }

    public boolean isDragging() {
        return mIsDragging;
    }

    public boolean isScaling() {
        return mDetector.isInProgress();
    }
}
