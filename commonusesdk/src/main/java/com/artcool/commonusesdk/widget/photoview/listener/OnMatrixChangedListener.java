package com.artcool.commonusesdk.widget.photoview.listener;

import android.graphics.RectF;

/**
 *
 * @author wuyibin
 * @date 2018/10/29
 */
public interface OnMatrixChangedListener {
    void onMatrixChanged(RectF rect);
}
