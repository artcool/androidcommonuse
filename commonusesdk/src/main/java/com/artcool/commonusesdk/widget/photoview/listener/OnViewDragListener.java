package com.artcool.commonusesdk.widget.photoview.listener;

/**
 *
 * @author wuyibin
 * @date 2018/10/29
 */
public interface OnViewDragListener {
    void onDrag(float dx,float dy);
}
