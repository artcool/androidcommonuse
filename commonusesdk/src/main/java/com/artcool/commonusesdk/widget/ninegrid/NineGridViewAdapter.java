package com.artcool.commonusesdk.widget.ninegrid;

import android.content.Context;
import android.widget.ImageView;

import com.artcool.commonusesdk.R;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author wuyibin
 * @date 2018/10/30
 */
public abstract class NineGridViewAdapter implements Serializable {
    private Context mContext;
    private List<ImageInfo> imageInfos;

    public NineGridViewAdapter(Context context,List<ImageInfo> list) {
        this.mContext = context;
        this.imageInfos = list;
    }

    /**
     * 如果要实现图片点击的逻辑，重写此方法即可
     * @param context       上下文
     * @param view          九宫格控件
     * @param index         索引
     * @param imageInfos    图片地址的数据集合
     */
    protected void onImageItemClick(Context context,NineGridView view,int index,List<ImageInfo> imageInfos){}

    /**
     * 生成ImageView容器的方式，默认使用NineGridImageViewWrapper类，即点击图片后，图片会有蒙板效果
     * 如果需要自定义图片展示效果，重写此方法即可
     *
     * @param context 上下文
     * @return 生成的 ImageView
     */
    protected ImageView generateImageView(Context context) {
        NineGridViewWrapper imageView = new NineGridViewWrapper(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageResource(R.drawable.default_color);
        return imageView;
    }

    public List<ImageInfo> getImageInfo() {
        return imageInfos;
    }

    public void setImageInfoList(List<ImageInfo> list) {
        this.imageInfos = list;
    }
}
