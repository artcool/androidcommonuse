package com.artcool.commonusesdk.widget.photoview.listener;

import android.view.MotionEvent;

/**
 *
 * @author wuyibin
 * @date 2018/10/29
 */
public interface OnSingleFlingListener {
    boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY);
}
