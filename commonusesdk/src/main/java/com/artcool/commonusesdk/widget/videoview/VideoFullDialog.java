package com.artcool.commonusesdk.widget.videoview;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.artcool.commonusesdk.R;
import com.artcool.commonusesdk.constant.SDKConstant;
import com.artcool.commonusesdk.core.video.VideoSlot;
import com.artcool.commonusesdk.module.VideoValue;
import com.artcool.commonusesdk.report.ReportManager;
import com.artcool.commonusesdk.utils.Utils;

/**
 *  全屏显示视频
 * @author wuyibin
 * @date 2018/11/20
 */
public class VideoFullDialog extends Dialog implements CustomVideoView.VideoPlayerListener {

    private Context mContext;
    private CustomVideoView mVideoView;
    private VideoValue mVideoValue;
    private int mPosition;
    private RelativeLayout mParentView;
    private ImageView mBackBtn;
    private RelativeLayout mRootView;
    //用于dialog出场动画
    private Bundle mEndBundle;
    private Bundle mStartBundle;
    //动画要执行的平移值
    private int deltaY;
    private VideoSlot.VideoSlotListener mSlotListener;

    public VideoFullDialog(Context context, CustomVideoView videoView, VideoValue instance, int position) {
        super(context, R.style.dialog_full_screen);
        this.mContext = context;
        this.mVideoView = videoView;
        this.mVideoValue = instance;
        this.mPosition = position;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.full_dialog_video_layout);
        initVideoView();
        initListening();
    }

    private void initListening() {
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickBackButton();
            }
        });
        mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickVideo();
            }
        });
    }

    public void setViewBundle(Bundle bundle) {
        this.mStartBundle = bundle;
    }

    /**
     * 初始化UI
     */
    private void initVideoView() {
        mParentView = findViewById(R.id.content_layout);
        mBackBtn = findViewById(R.id.player_close_btn);
        mRootView = findViewById(R.id.root_view);
        mRootView.setVisibility(View.VISIBLE);
        mVideoView.setVideoPlayerListener(this);
        mVideoView.mute(false);
        mParentView.addView(mVideoView);
        mParentView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mParentView.getViewTreeObserver().removeOnPreDrawListener(this);
                prepareScene();
                runEnterAnimation();
                return true;
            }
        });
    }

    /**
     * 准备入场动画
     */
    private void runEnterAnimation() {
        mVideoView.animate().setDuration(200)
                .setInterpolator(new LinearInterpolator())
                .translationY(0)
                .withStartAction(new Runnable() {
                    @Override
                    public void run() {
                        mRootView.setVisibility(View.VISIBLE);
                    }
                }).start();
    }

    @Override
    public void dismiss() {
        mParentView.removeView(mVideoView);
        super.dismiss();
    }

    /**
     * 准备动画所需的数据
     */
    private void prepareScene() {
        mEndBundle = Utils.getViewProperty(mVideoView);
        deltaY = mStartBundle.getInt(Utils.PROPNAME_SCREENLOCATION_TOP) - mEndBundle.getInt(Utils.PROPNAME_SCREENLOCATION_TOP);
        mVideoView.setTranslationY(deltaY);
    }

    @Override
    public void onBackPressed() {
        onClickBackButton();
    }

    @Override
    public void onBufferUpdate(int time) {
        try {
            if (mVideoValue != null) {
                ReportManager.suReport(mVideoValue.middleMonitor, time / SDKConstant.MILLION_UNIT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClickFullScreenButton() {
        onClickVideo();
    }

    @Override
    public void onClickVideo() {
        String desationUrl = mVideoValue.clickUrl;
        if (mSlotListener != null) {
            if (mVideoView.isFrameHidden() && !TextUtils.isEmpty(desationUrl)) {
                mSlotListener.onClickVideo(desationUrl);
                try {
                    ReportManager.pauseVideoReport(mVideoValue.clickMonitor,mVideoView.getCurrentPosition() / SDKConstant.MILLION_UNIT);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            //走默认样式
            if (mVideoView.isFrameHidden() && !TextUtils.isEmpty(desationUrl)) {
//                Intent intent = new Intent(mContext, AdBrowserActivity.class);
//                intent.putExtra(AdBrowserActivity.KEY_URL, mXAdInstance.clickUrl);
//                mContext.startActivity(intent);
                try {
                    ReportManager.pauseVideoReport(mVideoValue.clickMonitor, mVideoView.getCurrentPosition()
                            / SDKConstant.MILLION_UNIT);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClickBackButton() {
        runExitAnimator();
    }

    /**
     * 准备出场动画
     */
    private void runExitAnimator() {
        mVideoView.animate().setDuration(200)
                .setInterpolator(new LinearInterpolator())
                .translationY(deltaY)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        dismiss();

                    }
                }).start();
    }

    @Override
    public void onClickPlay() {

    }

    @Override
    public void onVideoLoadSuccess() {
        if (mVideoView != null) {
            mVideoView.resume();
        }
    }

    @Override
    public void onVideoLoadFailed() {

    }

    @Override
    public void onVideoLoadComplete() {
        try {
            int position = mVideoView.getDuration() / SDKConstant.MILLION_UNIT;
            ReportManager.sueReport(mVideoValue.endMonitor,true,position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        dismiss();
        if (mListener != null) {
            mListener.playComplete();
        }
    }

    public interface FullToSmallListener {
        void getCurrentPlayPosition(int position);

        //全屏播放结束时回调
        void playComplete();
    }

    private FullToSmallListener mListener;

    public void setFullToSmallListener(FullToSmallListener listener) {
        this.mListener = listener;
    }
}
