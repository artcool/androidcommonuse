package com.artcool.commonusesdk.widget.photoview.listener;

import android.view.View;

/**
 *
 * @author wuyibin
 * @date 2018/10/29
 */
public interface OnViewTapListener {
    void onViewTap(View view, float x, float y);
}
