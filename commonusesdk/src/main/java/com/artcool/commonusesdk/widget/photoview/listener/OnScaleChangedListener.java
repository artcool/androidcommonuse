package com.artcool.commonusesdk.widget.photoview.listener;

/**
 *
 * @author wuyibin
 * @date 2018/10/29
 */
public interface OnScaleChangedListener {
    void onScaleChange(float scaleFactor, float focusX, float focusY);
}
