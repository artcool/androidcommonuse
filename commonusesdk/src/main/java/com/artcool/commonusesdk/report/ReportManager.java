package com.artcool.commonusesdk.report;

import com.artcool.commonusesdk.constant.SDKConstant;
import com.artcool.commonusesdk.constant.SDKConstant.Params;
import com.artcool.commonusesdk.module.monitor.Monitor;
import com.artcool.commonusesdk.okhttp.OkHttpClientManager;
import com.artcool.commonusesdk.okhttp.listener.DisposeDataHandle;
import com.artcool.commonusesdk.okhttp.listener.DisposeDataListener;
import com.artcool.commonusesdk.okhttp.request.CommonRequest;
import com.artcool.commonusesdk.okhttp.request.RequestParams;
import com.artcool.commonusesdk.utils.Utils;

import java.util.ArrayList;

/**
 * 负责所有监测请求的发送
 * @author wuyibin
 * @date 2018/11/21
 */
public class ReportManager {

    /**
     * 默认的事件回调处理
     */
    private static DisposeDataHandle handle = new DisposeDataHandle(new DisposeDataListener() {
        @Override
        public void onSuccess(Object responseObj) {

        }

        @Override
        public void onFailure(Object reasonObj) {

        }
    });

    public static void susReport(ArrayList<Monitor> monitors,boolean isAuto) {
        if (monitors != null && monitors.size() > 0) {
            for (Monitor monitor : monitors) {
                RequestParams params = new RequestParams();
                if (Utils.containString(monitor.url,SDKConstant.ATM_PRE)) {
                    params.put("ve","0");
                    if (isAuto) {
                        params.put("auto","1");
                    }
                }
                OkHttpClientManager.get(CommonRequest.createMonitorRequest(monitor.url,params),handle);
            }
        }
    }

    /**
     * send the sueReoprt
     */
    public static void sueReport(ArrayList<Monitor> monitors, boolean isFull, long playTime) {
        if (monitors != null && monitors.size() > 0) {
            for (Monitor monitor : monitors) {
                RequestParams params = new RequestParams();
                if (Utils.containString(monitor.url, SDKConstant.ATM_PRE)) {
                    if (isFull) {
                        params.put("fu", "1");
                    }
                    params.put("ve", String.valueOf(playTime));
                }
                OkHttpClientManager.get(
                        CommonRequest.createMonitorRequest(monitor.url, params), handle);
            }
        }
    }

    /**
     * send the su report
     */
    public static void suReport(ArrayList<Monitor> monitors, long playTime) {
        if (monitors != null && monitors.size() > 0) {
            for (Monitor monitor : monitors) {
                RequestParams params = new RequestParams();
                if (monitor.time == playTime) {
                    if (Utils.containString(monitor.url, SDKConstant.ATM_PRE)) {
                        params.put("ve", String.valueOf(playTime));
                    }
                    OkHttpClientManager.get(
                            CommonRequest.createMonitorRequest(monitor.url, params), handle);
                }
            }
        }
    }

    /**
     * send the clicl full btn monitor
     *
     * @param monitors urls
     * @param playTime player time
     */
    public static void fullScreenReport(ArrayList<Monitor> monitors, long playTime) {
        if (monitors != null && monitors.size() > 0) {
            for (Monitor monitor : monitors) {
                RequestParams params = new RequestParams();
                if (Utils.containString(monitor.url, SDKConstant.ATM_PRE)) {
                    params.put("ve", String.valueOf(playTime));
                }
                OkHttpClientManager.get(
                        CommonRequest.createMonitorRequest(monitor.url, params), handle);
            }
        }
    }

    /**
     * send the click back full btn monitor
     *
     * @param monitors urls
     * @param playTime player time
     */
    public static void exitfullScreenReport(ArrayList<Monitor> monitors, long playTime) {
        if (monitors != null && monitors.size() > 0) {
            for (Monitor monitor : monitors) {
                RequestParams params = new RequestParams();
                if (Utils.containString(monitor.url, SDKConstant.ATM_PRE)) {
                    params.put("ve", String.valueOf(playTime));
                }
                OkHttpClientManager.get(
                        CommonRequest.createMonitorRequest(monitor.url, params), handle);
            }
        }
    }


    /**
     * send the video pause monitor
     *
     * @param monitors urls
     * @param playTime player time
     */
    public static void pauseVideoReport(ArrayList<Monitor> monitors, long playTime) {
        if (monitors != null && monitors.size() > 0) {
            for (Monitor monitor : monitors) {
                RequestParams params = new RequestParams();
                if (Utils.containString(monitor.url, SDKConstant.ATM_PRE)) {
                    params.put("ve", String.valueOf(playTime));
                }
                OkHttpClientManager.get(
                        CommonRequest.createMonitorRequest(monitor.url, params), handle);
            }
        }
    }

    /**
     * 发送广告是否正常解析及展示监测
     */
    public static void sendAdMonitor(boolean isPad, String sid, String ie, String appVersion, Params step, String result) {
        RequestParams params = new RequestParams();
        params.put(Params.lvs.getKey(), Params.lvs.getValue());
        params.put(Params.st.getKey(), Params.st.getValue());
        params.put(Params.os.getKey(), Params.os.getValue());
        params.put(Params.p.getKey(), Params.p.getValue());
        params.put(Params.appid.getKey(), Params.appid.getValue());
        if (isPad) {
            params.put(Params.bt_pad.getKey(), Params.bt_pad.getValue());
        } else {
            params.put(Params.bt_phone.getKey(), Params.bt_phone.getValue());
        }
        params.put(step.getKey(),
                step.getValue());
        params.put(SDKConstant.STEP_CD, result);
        params.put(SDKConstant.SID, sid);
        params.put(SDKConstant.IE, ie);
        params.put(SDKConstant.AVS, appVersion);

        OkHttpClientManager.get(CommonRequest.createGetRequest(SDKConstant.ATM_MONITOR, params), handle);
    }

}
