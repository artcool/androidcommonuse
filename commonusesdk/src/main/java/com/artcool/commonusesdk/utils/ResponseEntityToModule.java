package com.artcool.commonusesdk.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * 递归ORM映射(有反射，不能被混淆)
 * Created by wuyibin on 2018/10/26.
 */
public class ResponseEntityToModule {

    public static Object parseJsonModule(String jsonContent,Class<?> clazz) {
        Object moduleObj = null;
        try {
            JSONObject jsonObject = new JSONObject(jsonContent);
            moduleObj = parseJsonObjectToModule(jsonObject,clazz);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return moduleObj;
    }

    public static Object parseJsonObjectToModule(JSONObject jsonObject, Class<?> clazz) {
        Object moduleObj = null;
        try {
            moduleObj = clazz.newInstance();
            setFieldValue(moduleObj,jsonObject,clazz);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return moduleObj;
    }

    private static void setFieldValue(Object moduleObj, JSONObject jsonObject, Class<?> clazz) throws JSONException, IllegalAccessException, InstantiationException {
        if (clazz.getSuperclass() != null) {
            setFieldValue(moduleObj,jsonObject,clazz.getSuperclass());
        }
        Field[] fields = clazz.getDeclaredFields();
        Class<?> cls;
        String name;
        for (Field f : fields) {
            f.setAccessible(true);
            cls = f.getType();
            name = f.getName();
            if (!jsonObject.has(name) || jsonObject.isNull(name)) {
                continue;
            }
            if (cls.isPrimitive() || isWrappedPrimitive(cls)) {
                setPrimitiveFieldValue(f,moduleObj,jsonObject.get(name));
            } else {
                if (cls.isAssignableFrom(String.class)) {
                    f.set(moduleObj,String.valueOf(ArrayList.class));
                } else if (cls.isAssignableFrom(ArrayList.class)) {
                    parseJsonArrayToList(f,name,moduleObj,jsonObject);
                }
                //在这儿可以加个判断，如果是RealmList类型则转化为RealmList,方便直接存入数据库
//                else if (cls.isAssignableFrom(RealmList.class)) {
//                    parseJsonArray
//                }
                else {
                    Object obj = parseJsonObjectToModule(jsonObject.getJSONObject(name), cls.newInstance().getClass());
                    f.set(moduleObj,obj);
                }
            }
        }
    }

    public static ArrayList<Object> parseJsonArrayToList(Field f, String name, Object moduleObj, JSONObject jsonObject) throws JSONException, IllegalAccessException {
        ArrayList<Object> objList = new ArrayList<>();
        Type fc = f.getGenericType();
        if (fc instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) fc;
            if (pt.getActualTypeArguments()[0] instanceof Class) {
                Class<?> clss  = (Class<?>) pt.getActualTypeArguments()[0];
                if (jsonObject.get(name) instanceof JSONArray) {
                    JSONArray array = jsonObject.getJSONArray(name);
                    for (int i = 0; i < array.length(); i++) {
                        if (array.get(i) instanceof JSONObject) {
                            objList.add(parseJsonObjectToModule(array.getJSONObject(i),clss));
                        } else {
                            if (clss.isAssignableFrom(array.get(i).getClass())) {
                                objList.add(array.get(i));
                            }
                        }
                    }
                }
                f.set(moduleObj,objList);
            }
        }
        return objList;
    }

    private static void setPrimitiveFieldValue(Field f, Object moduleObj, Object o) throws IllegalAccessException {
        if (f.getType().isAssignableFrom(o.getClass())) {
            f.set(moduleObj,o);
        } else {
            f.set(moduleObj,makeTypeSafeValue(f.getType(),o.toString()));
        }
    }

    private static Object makeTypeSafeValue(Class<?> type, String value) {
        if (int.class == type || Integer.class == type) {
            return Integer.parseInt(value);
        } else if (long.class == type || Long.class == type) {
            return Long.parseLong(value);
        } else if (short.class == type || Short.class == type) {
            return Short.parseShort(value);
        } else if (char.class == type || Character.class == type) {
            return value.charAt(0);
        } else if (byte.class == type || Byte.class == type) {
            return Byte.valueOf(value);
        } else if (float.class == type || Float.class == type) {
            return Float.parseFloat(value);
        } else if (double.class == type || Double.class == type) {
            return Double.parseDouble(value);
        } else if (boolean.class == type || Boolean.class == type) {
            return Boolean.valueOf(value);
        } else {
            return value;
        }
    }

    private static boolean isWrappedPrimitive(Class<?> cls) {
        if (cls.getName().equals(Boolean.class.getName()) || cls.getName().equals(Byte.class.getName())
                || cls.getName().equals(Character.class.getName()) || cls.getName().equals(Short.class.getName())
                || cls.getName().equals(Integer.class.getName()) || cls.getName().equals(Long.class.getName())
                || cls.getName().equals(Float.class.getName()) || cls.getName().equals(Double.class.getName())) {
            return true;
        }
        return false;
    }

}
