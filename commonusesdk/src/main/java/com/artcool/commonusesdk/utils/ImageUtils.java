package com.artcool.commonusesdk.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;

import com.orhanobut.logger.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *  图片相关操作类
 * @author wuyibin
 * @date 2019/7/30
 */
public class ImageUtils {

    private Context mContext;

    public static boolean saveImageToAlbum(Context context, Bitmap bitmap) {
        String storePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "acu";
        File dir = new File(storePath);
        if (!dir.exists()) {
            dir.mkdir();
        }
        String fileName = System.currentTimeMillis() + ".jpg";
        File file = new File(dir,fileName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            boolean isSuccess = bitmap.compress(Bitmap.CompressFormat.JPEG, 60, fos);
            fos.flush();
            fos.close();
            Uri uri = Uri.fromFile(file);
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,uri));
            if (isSuccess) {
                return true;
            }
            return false;
        } catch (IOException e) {
            Logger.d(e);
            e.printStackTrace();
        }
        return false;
    }
}
