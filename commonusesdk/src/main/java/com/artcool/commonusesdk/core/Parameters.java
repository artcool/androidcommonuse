package com.artcool.commonusesdk.core;

import com.artcool.commonusesdk.constant.SDKConstant;

/**
 *  SDK全局参数配置，都用静态来保证统一性
 * @author wuyibin
 * @date 2018/11/16
 */
public class Parameters {

    /**
     * 默认都可以自动播放
     */
    private static SDKConstant.AutoPlaySetting currentSetting = SDKConstant.AutoPlaySetting.AUTO_PLAY_3G_4G_WIFI;

    public static void setCurrentSetting(SDKConstant.AutoPlaySetting setting) {
        currentSetting = setting;
    }

    public static SDKConstant.AutoPlaySetting getCurrentSetting() {
        return currentSetting;
    }

    public static String getSDKVersion() {
        return "1.0.0";
    }

}
