package com.artcool.commonusesdk.core.video;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.artcool.commonusesdk.R;
import com.artcool.commonusesdk.constant.SDKConstant;
import com.artcool.commonusesdk.core.Parameters;
import com.artcool.commonusesdk.module.VideoValue;
import com.artcool.commonusesdk.report.ReportManager;
import com.artcool.commonusesdk.utils.Utils;
import com.artcool.commonusesdk.widget.videoview.CustomVideoView;
import com.artcool.commonusesdk.widget.videoview.VideoFullDialog;
import com.artcool.commonusesdk.zxing.util.Util;

/**
 *  业务逻辑层
 * @author wuyibin
 * @date 2018/11/21
 */
public class VideoSlot implements CustomVideoView.VideoPlayerListener {

    private VideoValue instance;
    private Context mContext;

    /**
     * UI
     */
    private CustomVideoView mVideoView;
    private ViewGroup mParentView;

    /**
     * 是否可自动暂停标志位
     */
    private boolean canPause = false;

    /**
     * 防止将要滑入滑出时播放器的状态改变
     */
    private int lastArea = 0;

    public VideoSlot(VideoValue videoValue, VideoSlotListener slotListener, CustomVideoView.FrameImageLoaderListener frameImageLoaderListener) {
        this.instance = videoValue;
        this.videoSlotListener = slotListener;
        this.mParentView = slotListener.getAdParent();
        this.mContext = mParentView.getContext();
        initVideoView(frameImageLoaderListener);
    }

    private void initVideoView(CustomVideoView.FrameImageLoaderListener frameImageLoaderListener) {
        mVideoView = new CustomVideoView(mContext,mParentView);
        if (instance != null) {
            mVideoView.setDataSource(instance.resource);
            mVideoView.setFrameURI(instance.thumb);
            mVideoView.setOnFrameImageLoaderListener(frameImageLoaderListener);
            mVideoView.setVideoPlayerListener(this);
        }
        RelativeLayout paddingView = new RelativeLayout(mContext);
        paddingView.setBackgroundColor(mContext.getResources().getColor(R.color.black));
        paddingView.setLayoutParams(mVideoView.getLayoutParams());
        mParentView.addView(paddingView);
        mParentView.addView(mVideoView);
    }

    private boolean isPlaying() {
        if (mVideoView != null) {
            return mVideoView.isPlaying();
        }
        return false;
    }

    private boolean isRealPause() {
        if (mVideoView != null) {
            return mVideoView.isRealPause();
        }
        return false;
    }

    private boolean isComplete() {
        if (mVideoView != null) {
            return mVideoView.isComplete();
        }
        return false;
    }

    private void pauseVideo(boolean isAuto) {
        if (mVideoView != null) {
            if (isAuto) {
                //发自动暂停监测
                if (!isRealPause() && isPlaying()) {
                    try {
                        ReportManager.pauseVideoReport(instance.event.pause.content,getPosition());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * resume the video
     */
    private void resumeVideo() {
        if (mVideoView != null) {
            mVideoView.resume();
            if (isPlaying()) {
                sendSUSReport(true);
            }
        }
    }

    public void updateInScrollView() {
        int currentArea = Utils.getVisiblePercent(mParentView);
        //小于0表示未出现在屏幕上，不做任何处理
        if (currentArea < 0) {
            return;
        }
        //刚要滑入和滑出时，异常状态的处理
        if (Math.abs(currentArea - lastArea) >= 100) {
            return;
        }
        if (currentArea < SDKConstant.VIDEO_SCREEN_PERCENT) {
            //进入自动暂停状态
            if (canPause) {
                pauseVideo(true);
                canPause = false;
            }
            lastArea = 0;
            //滑动出50%后标记为从头开始播
            mVideoView.setIsComplete(false);
            mVideoView.setIsRealPause(false);
            return;
        }
        if (isRealPause() || isComplete()) {
            //进入手动暂停或者播放结束，播放结束和不满足自动播放条件都作为手动暂停
            pauseVideo(false);
            canPause = false;
            return;
        }
        if (Utils.canAutoPlay(mContext,Parameters.getCurrentSetting()) || isPlaying()) {
            lastArea = currentArea;
            resumeVideo();
            canPause = true;
            mVideoView.setIsRealPause(false);
        } else {
            pauseVideo(false);
            //不能自动播放则设置为手动暂停效果
            mVideoView.setIsRealPause(true);
        }
    }

    public void destroy() {
        mVideoView.destroy();
        mVideoView = null;
        mContext = null;
        instance = null;
    }


    private int getPosition() {
        return mVideoView.getCurrentPosition() / SDKConstant.MILLION_UNIT;
    }

    @Override
    public void onBufferUpdate(int time) {
        try {
            ReportManager.suReport(instance.middleMonitor,time / SDKConstant.MILLION_UNIT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClickFullScreenButton() {
        try {
            ReportManager.fullScreenReport(instance.event.full.content,getPosition());
        } catch (Exception e) {
            e.printStackTrace();
        }
        //获取videoview在当前界面的属性
        Bundle bundle = Utils.getViewProperty(mParentView);
        mParentView.removeView(mVideoView);
        VideoFullDialog dialog = new VideoFullDialog(mContext, mVideoView, instance, mVideoView.getCurrentPosition());
        dialog.setFullToSmallListener(new VideoFullDialog.FullToSmallListener() {
            @Override
            public void getCurrentPlayPosition(int position) {
                backToSmallMode(position);
            }

            @Override
            public void playComplete() {
                bigPlayComplete();
            }
        });
    }

    private void bigPlayComplete() {
        if (mVideoView.getParent() == null) {
            mParentView.addView(mVideoView);
        }
        mVideoView.setTranslationY(0);
        mVideoView.isShowFullBtn(true);
        mVideoView.mute(true);
        mVideoView.setVideoPlayerListener(this);
        mVideoView.seekAndPause(0);
        canPause = false;
    }

    private void backToSmallMode(int position) {
        if (mVideoView.getParent() == null) {
            mParentView.addView(mVideoView);
        }
        //防止动画导致偏离父容器
        mVideoView.setTranslationY(0);
        mVideoView.isShowFullBtn(true);
        mVideoView.mute(true);
        mVideoView.setVideoPlayerListener(this);
        mVideoView.seekAndResume(position);
        //标为可自动暂停
        canPause = true;
    }

    @Override
    public void onClickVideo() {
        String desationUrl = instance.clickUrl;
        if (videoSlotListener != null) {
            if (mVideoView.isFrameHidden() && !TextUtils.isEmpty(desationUrl)) {
                videoSlotListener.onClickVideo(desationUrl);
                try {
                    ReportManager.pauseVideoReport(instance.clickMonitor,mVideoView.getCurrentPosition() / SDKConstant.MILLION_UNIT);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (mVideoView.isFrameHidden() && !TextUtils.isEmpty(desationUrl)) {
                //走默认样式
                if (mVideoView.isFrameHidden() && !TextUtils.isEmpty(desationUrl)) {
//                    Intent intent = new Intent(mContext, BrowserActivity.class);
//                    intent.putExtra(BrowserActivity.KEY_URL, instance.clickUrl);
//                    mContext.startActivity(intent);
                    try {
                        ReportManager.pauseVideoReport(instance.clickMonitor, mVideoView.getCurrentPosition()
                                / SDKConstant.MILLION_UNIT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onClickBackButton() {

    }

    @Override
    public void onClickPlay() {
        sendSUSReport(false);
    }

    @Override
    public void onVideoLoadSuccess() {
        if (videoSlotListener != null) {
            videoSlotListener.onVideoLoadSuccess();
        }
    }

    @Override
    public void onVideoLoadFailed() {
        if (videoSlotListener != null) {
            videoSlotListener.onVideoLoadFailed();
        }
        //加载失败全部回到初始状态
        canPause = false;
    }

    @Override
    public void onVideoLoadComplete() {
        try {
            ReportManager.sueReport(instance.endMonitor,false,getDuration());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (videoSlotListener != null) {
            videoSlotListener.onVideoLoadComplete();
        }
        mVideoView.setIsRealPause(true);
    }

    private int getDuration() {
        return mVideoView.getDuration() / SDKConstant.MILLION_UNIT;
    }

    /**
     * 发送视频开始播放监测
     * @param isAuto
     */
    private void sendSUSReport(boolean isAuto) {
        try {
            ReportManager.susReport(instance.startMonitor,isAuto);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 传递消息到appcontext层
     */
    public interface VideoSlotListener {
         ViewGroup getAdParent();

        void onVideoLoadSuccess();

        void onVideoLoadFailed();

        void onVideoLoadComplete();

        void onClickVideo(String url);
    }

    private VideoSlotListener videoSlotListener;

}
