package com.artcool.commonusesdk.okhttp.request;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 请求参数封装
 * Created by wuyibin on 2018/10/25.
 */
public class RequestParams {

    public ConcurrentHashMap<String,String> urlParams = new ConcurrentHashMap<>();
    public ConcurrentHashMap<String,Object> fileParame = new ConcurrentHashMap<>();

    /**
     * 创建一个空参构造函数初始化
     */
    public RequestParams() {
        this((Map<String,String>)null);
    }

    public RequestParams(Map<String, String> source) {
        if (source != null) {
            for (Map.Entry<String,String> entry : source.entrySet()) {
                put(entry.getKey(),entry.getValue());
            }
        }
    }

    public RequestParams(final String key, final String value) {
        this(new HashMap<String, String>(){
            {
                put(key,value);
            }
        });
    }

    /**
     * 添加参数
     * @param key   键
     * @param value 值
     */
    public void put(String key, String value) {
        if (key != null && value != null) {
            urlParams.put(key,value);
        }
    }

    public void put(String key,Object object) {
        if (key != null) {
            fileParame.put(key,object);
        }
    }

    /**
     * 判断是否有参数
     * @return
     */
    public boolean hasParams() {
        if (urlParams.size() > 0 || fileParame.size() > 0) {
            return true;
        }
        return false;
    }

}
