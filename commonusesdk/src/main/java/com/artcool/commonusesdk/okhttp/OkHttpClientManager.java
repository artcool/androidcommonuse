package com.artcool.commonusesdk.okhttp;

import com.artcool.commonusesdk.okhttp.cookie.SimpleCookieJar;
import com.artcool.commonusesdk.okhttp.https.HttpsUtils;
import com.artcool.commonusesdk.okhttp.listener.DisposeDataHandle;
import com.artcool.commonusesdk.okhttp.response.CommonFileCallBack;
import com.artcool.commonusesdk.okhttp.response.CommonJsonCallBack;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.Call;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by wuyibin on 2018/10/25.
 */
public class OkHttpClientManager {

    //设置超时时间
    private static final int TIME_OUT = 30;
    private static OkHttpClient mOkHttpClient;

    static {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        //支持https
        builder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                return true;
            }
        });

        /**
         * 为所以请求添加请求头
         */
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request()
                                    .newBuilder()
                                    //标明本次请求的客户端
                                    .addHeader("User-Agent","artcool")
                                    .build();
                return chain.proceed(request);
            }
        });

        builder.cookieJar(new SimpleCookieJar());
        builder.connectTimeout(TIME_OUT, TimeUnit.SECONDS);
        builder.readTimeout(TIME_OUT, TimeUnit.SECONDS);
        builder.writeTimeout(TIME_OUT, TimeUnit.SECONDS);
        builder.followRedirects(true);

        builder.sslSocketFactory(HttpsUtils.initSSLSocketFactory(),HttpsUtils.initTrustManager());

        mOkHttpClient = builder.build();
    }

    public static OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }


    /**
     *
     * @param request
     * @param handle
     * @return
     */
    public static Call get(Request request, DisposeDataHandle handle) {
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new CommonJsonCallBack(handle));
        return call;
    }

    public static Call post(Request request,DisposeDataHandle handle) {
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new CommonJsonCallBack(handle));
        return call;
    }

    public static Call downloadFile(Request request,DisposeDataHandle handle) {
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new CommonFileCallBack(handle));
        return call;
    }

}
