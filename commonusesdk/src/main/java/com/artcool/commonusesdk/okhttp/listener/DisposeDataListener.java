package com.artcool.commonusesdk.okhttp.listener;

public interface DisposeDataListener {

    /**
     * 请求成功回调事件处理
     * @param responseObj
     */
    void onSuccess(Object responseObj);

    /**
     * 请求失败回调时间处理
     * @param reasonObj
     */
    void onFailure(Object reasonObj);
}
