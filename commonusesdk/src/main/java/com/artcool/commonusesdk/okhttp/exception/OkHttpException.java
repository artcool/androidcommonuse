package com.artcool.commonusesdk.okhttp.exception;

public class OkHttpException extends Exception {
    private static final long serialVersionUID = 1L;
    /**
     * 服务器返回状态码
     */
    private int code;
    /**
     *  服务器返回错误信息
     */
    private Object errMsg;

    public OkHttpException(int code, Object message1) {
        this.code = code;
        this.errMsg = message1;
    }

    public int getCode() {
        return code;
    }

    public Object getErrMsg() {
        return errMsg;
    }
}
