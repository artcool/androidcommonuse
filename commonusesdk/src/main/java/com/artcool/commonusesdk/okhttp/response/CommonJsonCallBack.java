package com.artcool.commonusesdk.okhttp.response;

import android.os.Handler;
import android.os.Looper;

import com.artcool.commonusesdk.okhttp.exception.OkHttpException;
import com.artcool.commonusesdk.okhttp.listener.DisposeDataHandle;
import com.artcool.commonusesdk.okhttp.listener.DisposeDataListener;
import com.artcool.commonusesdk.utils.ResponseEntityToModule;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.Response;

public class CommonJsonCallBack implements Callback {

    /**
     * 逻辑层异常处理，每个项目的定义可以不一样
     */
    protected final String RESULT_CODE = "ecode";
    protected final int RESULT_CODE_VALUE = 0;
    protected final String ERROR_MSG = "emsg";
    protected final String EMPTY_MSG = "";
    protected final String COOKIE_STORE = "Set-Cookie";

    /**
     * 服务器异常
     */
    protected final int NETWORK_ERROR = -1;
    protected final int JSON_ERROR = -1;
    protected final int OTHER_ERROR = -1;

    /**
     * 将其他线程的数据转发到UI线程
     */
    private Handler mDeliverHandler;
    private DisposeDataListener mListener;
    private Class<?> mClass;

    public CommonJsonCallBack(DisposeDataHandle handle) {
        this.mDeliverHandler = new Handler(Looper.getMainLooper());
        this.mListener = handle.mListener;
        this.mClass = handle.mClass;
    }

    @Override
    public void onFailure(Call call, final IOException e) {
        mDeliverHandler.post(new Runnable() {
            @Override
            public void run() {
                mListener.onFailure(new OkHttpException(NETWORK_ERROR,e));
            }
        });
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        final String result = response.body().string();
        List<String> cookieLists = handleCookie(response.headers());
        mDeliverHandler.post(new Runnable() {
            @Override
            public void run() {
                handleResponse(result);
            }
        });

    }

    private void handleResponse(Object responseObj) {
        if (responseObj == null || responseObj.toString().trim().equals("")) {
            mListener.onFailure(new OkHttpException(NETWORK_ERROR,EMPTY_MSG));
            return;
        }
        try {
            JSONObject result = new JSONObject(responseObj.toString());
            if (mClass == null) {
                mListener.onSuccess(result);
            } else {
                Object obj = ResponseEntityToModule.parseJsonObjectToModule(result, mClass);
                if (obj != null) {
                    mListener.onSuccess(obj);
                } else {
                    mListener.onFailure(new OkHttpException(JSON_ERROR,EMPTY_MSG));
                }
            }
        } catch (JSONException e) {
            mListener.onFailure(new OkHttpException(OTHER_ERROR,e.getMessage()));
            e.printStackTrace();
        }
    }

    /**
     * 处理cookie缓存
     * @param headers
     * @return
     */
    private List<String> handleCookie(Headers headers) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < headers.size(); i++) {
            if (headers.name(i).equalsIgnoreCase(COOKIE_STORE)) {
                list.add(headers.value(i));
            }
        }
        return list;
    }
}
