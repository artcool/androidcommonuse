package com.artcool.commonusesdk.okhttp.listener;

/**
 *  监听下载进度
 * @author wuyibin
 * @date 2018/10/26
 */
public interface DisposeDownloadListener extends DisposeDataListener {
    void onProgress(int progress);
}
