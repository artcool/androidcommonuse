package com.artcool.commonusesdk.okhttp.request;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * 请求类型封装
 * Created by wuyibin on 2018/10/25.
 */
public class CommonRequest {

    /**
     * 没有请求头
     * @param url
     * @param params
     * @return
     */
    public static Request createPostReques(String url,RequestParams params) {
        return createPostReques(url, params,null);
    }

    /**
     * 可以带请求头的post请求
     * @param url
     * @param params
     * @param headers
     * @return
     */
    private static Request createPostReques(String url, RequestParams params, RequestParams headers) {
        FormBody.Builder mFormBodyBuild = new FormBody.Builder();
        //添加请求体参数
        if (params != null) {
            //如果参数不为空，则添加请求参数
            for (Map.Entry<String,String> entry : params.urlParams.entrySet()) {
                mFormBodyBuild.add(entry.getKey(),entry.getValue());
            }
        }

        Headers.Builder mHeaderBuild = new Headers.Builder();
        //添加请求头参数
        if (headers != null) {
            for (Map.Entry<String,String> entry : headers.urlParams.entrySet()) {
                mHeaderBuild.add(entry.getKey(),entry.getValue());
            }
        }

        FormBody mFormBody = mFormBodyBuild.build();
        Headers mHeader = mHeaderBuild.build();
        Request request = new Request.Builder().url(url).post(mFormBody).headers(mHeader).build();
        return request;
    }

    /**
     * get请求
     * @param url
     * @param params
     * @return
     */
    public static Request createGetRequest(String url,RequestParams params) {
        return createGetRequest(url,params,null);
    }

    /**
     * 带请求头的get请求
     * @param url
     * @param params
     * @param headers
     * @return
     */
    private static Request createGetRequest(String url, RequestParams params, RequestParams headers) {
        StringBuilder urlBuilder = new StringBuilder(url).append("?");
        if (params != null) {
            for (Map.Entry<String,String> entry : params.urlParams.entrySet()) {
                urlBuilder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }

        Headers.Builder mHeaderBuilder = new Headers.Builder();
        if (headers != null) {
            for (Map.Entry<String,String> entry : headers.urlParams.entrySet()) {
                mHeaderBuilder.add(entry.getKey(),entry.getValue());
            }
        }
        Headers header = mHeaderBuilder.build();
        Request request = new Request.Builder().
                                url(urlBuilder.substring(0,urlBuilder.length() - 1))
                                .get()
                                .headers(header)
                                .build();
        return request;
    }

    /**
     * 创建模拟get请求
     * @param url
     * @param params
     * @return
     */
    public static Request createMonitorRequest(String url,RequestParams params) {
        StringBuilder urlBuilder = new StringBuilder(url).append("&");
        if (params != null && params.hasParams()) {
            for (Map.Entry<String,String> entry : params.urlParams.entrySet()) {
                urlBuilder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }
        return new Request.Builder().url(urlBuilder.substring(0,urlBuilder.length() - 1)).get().build();
    }

    /**
     * 文件上传请求
     */
    private static final MediaType FILE_TYPE = MediaType.parse("application/octet-stream");

    public static Request createMultiPostRequest(String url,RequestParams params) {
        MultipartBody.Builder requestBody = new MultipartBody.Builder();
        requestBody.setType(MultipartBody.FORM);
        if (params != null) {
            for (Map.Entry<String,Object> entry : params.fileParame.entrySet()) {
                if (entry.getValue() instanceof File) {
                    requestBody.addPart(Headers.of("Content-Disposition","form-data; name=\"" + entry.getKey() + "\""),RequestBody.create(FILE_TYPE, (File) entry.getValue()));
                } else if (entry.getValue() instanceof String) {
                    requestBody.addPart(Headers.of("Content-Disposition","form-data; name=\"" + entry.getKey() + "\""),RequestBody.create(null, (File) entry.getValue()));
                }
            }
        }
        return new Request.Builder().url(url).post(requestBody.build()).build();
    }
}
