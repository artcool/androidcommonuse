# AndroidCommonUse

Android通用框架

1、首页框架采用BottomNavigationView+Fragment实现

2、网络请求框架采用OKHTTP实现

3、异步图片加载框架采用ImageLoader实现

4、二维码扫描及生成采用zxing实现，注意使用的时候应该动态申请权限，否则会报错permission deny 导致can't find camare

5、MediaPlayer+TextureView实现自定义视频播放器

自定义控件

ImageTextView ：采用vertical布局

RoundImageView ：圆形图片

RoundImageTextView : 圆形头像+主标题+副标题

添加自定义搜索框    仿Google设计
SearchView 和 SearchBar

photoView:图片操作组件

ninegridview:多图显示控件，采用九宫格模式

switchButton:开关按钮，处理状态变化